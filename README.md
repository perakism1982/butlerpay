API serving mobile app for online ordering food 

CMS for modifying mobile app content, monitoring orders, generating basic reports

Automating tasks with cron or celery|supervisor

Basic Libs used apart from Python core modules:
-Flask and extensions (micro webframework)
-Flask Sqlalchemy
-Celery (asynchronous task queue/job queue)
-Requests (HTTP)