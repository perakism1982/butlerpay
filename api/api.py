from flask import Flask, jsonify, url_for, redirect, render_template, g, flash, abort, send_file
from apscheduler.schedulers.background import BackgroundScheduler
from collections import OrderedDict


import pwd
import grp
import os
import sys
sys.path.append("/var/www/butlerpay/api-wrapper")
sys.path.append("/var/www/butlerpay/api-wrapper/api")

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from dbsee import ee_payment
import random, time, urllib
from decimal import Decimal
from celery import Celery

import uuid

import time
import datetime

from datetime import datetime, timedelta

import requests
import xmltodict
import json
import urlparse

import importlib
import string

from flask import make_response, request, current_app
from functools import wraps, update_wrapper
import flask_login
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug.utils import secure_filename

import logging
from logging.handlers import TimedRotatingFileHandler
from logging import Formatter

#import sentry_sdk
#from sentry_sdk.integrations.flask import FlaskIntegration

#sentry_sdk.init(
    #dsn="https://sentry_sdk_example",
    #integrations=[FlaskIntegration()]
#)

def make_celery(app):
    celery_foodfare = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery_foodfare.conf.update(app.config)
    TaskBase = celery_foodfare.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery_foodfare.Task = ContextTask
    return celery_foodfare


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:******@localhost/foodfare2'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

UPLOAD_FOLDER = '/var/www/butlerpay/api-wrapper/api/static/uploads/'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.config.update(
	CELERY_BROKER_URL='redis://localhost:6379/0',
    CELERY_RESULT_BACKEND='redis://localhost:6379/0'
)

def allowed_file(filename):
	return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

login_manager = flask_login.LoginManager()
login_manager.init_app(app)

celery_foodfare = make_celery(app)

db = SQLAlchemy(app)
from models import *
import models
db.create_all()


API_PAYLAH = "http://pay.butlerpad.com/api/payment"
FOODFARE = "https://uat-mercury.plus.com.sg/oneloyalty/services/OneLoyaltyAxisService?wsdl"
SENDGRID_API_KEY = "**************************************"

## Logging
#logHandler = TimedRotatingFileHandler('/tmp/foodfare2_logs.log',
                                   #when='midnight',
                                   #interval=1,
                                   #backupCount=90)
#logHandler.setFormatter(Formatter('{%(message)s}'))
#logHandler.setLevel(logging.INFO)
#app.logger.setLevel(logging.INFO)
#app.logger.addHandler(logHandler)


##############################
# Cron Tasks
##############################
def check_foodfare2_stalls():
	print "Foodfare Scheduler is alive!"
	time_now = datetime.datetime.now().time()
	stalls = FoodfareStall.query.all()
	for stall in stalls:
		if stall.auto_open_close is True:
			if stall.opens_at in [None, 'null', '']:
				print 'Foodfare stall #{stall_id} undefined opening time'.format(stall_id=stall.id)
				continue
			if stall.closes_at in [None, 'null', '']:
				print 'Foodfare stall #{stall_id} undefined closing time'.format(stall_id=stall.id)
				continue

			opens_at = datetime.datetime.strptime(stall.opens_at, "%H:%M").time()
			closes_at = datetime.datetime.strptime(stall.closes_at, "%H:%M").time()
			if opens_at < time_now < closes_at:
				if stall.shown is False:
					stall.shown = True
					db.session.commit()
					print "Foodfare stall #{stall_id} opened".format(stall_id=stall.id)
			else:
				if stall.shown is True:
					stall.shown = False
					db.session.commit()
					print "Foodfare stall #{stall_id} closed".format(stall_id=stall.id)
					unread_orders = FoodfareOrder.query.filter_by(unread=True).all()
					for o in unread_orders:
						o.unread = False
						db.session.commit()
						print "Foodfare Order #{order_id} marked as read".format(order_id=o.id)
	db.session.close()


def check_foodfare2_promo_config_date():
	today = datetime.datetime.now().date()
	promo_configs = FoodfarePromoConfig.query.filter_by(active=True).all()
	for promo_config in promo_configs:
		print "Promo Configuration checking..."
		if promo_config.ends_at:
			promo_config_ends_at = datetime.datetime.strptime(promo_config.ends_at, '%Y-%m-%d').date()
			if promo_config_ends_at >= today:
				promo_config.active = False
				db.session.commit()
				print "Foodfare Promo Configuration #{promo_config} inactivated".format(promo_config=promo_config.id)
	db.session.close()


def check_foodfare2_promo_config_num():
	promo_configs = FoodfarePromoConfig.query.filter_by(active=True).all()
	for promo_config in promo_configs:
		print "Promo Configuration checking..."
		promos_used_num = FoodfareOrder.query.filter_by(promo_id=promo_config.id).count()
		if promos_used_num >= promo_config.issued:
			promo_config.active = False
			db.session.commit()
			print "Promo Configuration {promo_config} inactivated".format(promo_config=promo_config.id)
	db.session.close()

sched = BackgroundScheduler(daemon=True)
sched.add_job(check_foodfare2_promo_config_date, 'cron', day='*')
sched.add_job(check_foodfare2_promo_config_num, 'cron', minute='*/10')
sched.add_job(check_foodfare2_stalls, 'cron', minute='*/10')
sched.start()




##############################
# Helper Functions
##############################

def get_guest_from_api_call():
	user_token = request.args.get("usertoken")
	if user_token is None:
		return None
	return FoodfareUser.query.filter_by(token=user_token).first()

def HMAC_SHA256(key, signature_string):

	import hashlib
	import hmac
	import base64
	signature = base64.b64encode(hmac.new(key, signature_string, digestmod=hashlib.sha256).digest())
	return signature


def send_slack(message):
	try:
		import urllib2
		WEBHOOK = "https://hooks.slack.com/services/random_webhook"
		urllib2.urlopen(WEBHOOK, data=json.dumps({"text":message})).read()
	except Exception, e:
		print "Slack sending failed " + str(e)


def api_response(ret=None, error=None, prompt_signin=False, foodfare_log_id=None):
	if prompt_signin:
		return jsonify(ret=ret, error=error,success=(error == None),prompt_signin=True)
	else:
		if 'foodfare/' in request.path:
			if error:
				suffix = ""
				if foodfare_log_id:
					suffix += " (" + str(make_foodfare_log_url(foodfare_log_id)) + ")"
				try:
					if error["foodfare_req_status"]:
						suffix += " http" + str(error["foodfare_req_status"])
				except:
					pass
				send_slack("API request failed: " + str(request.url) + suffix)

			if request.args.get("source") == "statuscake" and error:
				if foodfare_log_id is None:
					return jsonify(ret=ret, error=error, success=(error == None)), 500
				return jsonify(ret=ret, error=error, success=(error == None),
					foodfare_log=make_foodfare_log_url(foodfare_log_id)), 500
			else:
				if foodfare_log_id is None:
					return jsonify(ret=ret, error=error, success=(error == None))
				return jsonify(ret=ret, error=error, success=(error == None),
					foodfare_log=make_foodfare_log_url(foodfare_log_id))
		return jsonify(ret=ret, error=error, success=(error == None))


# source: http://flask.pocoo.org/snippets/56/
def crossdomain(origin=None, methods=None, headers=None,
				max_age=21600, attach_to_all=True,
				automatic_options=True):
	if methods is not None:
		methods = ', '.join(sorted(x.upper() for x in methods))
	if headers is not None and not isinstance(headers, basestring):
		headers = ', '.join(x.upper() for x in headers)
	if not isinstance(origin, basestring):
		origin = ', '.join(origin)
	if isinstance(max_age, timedelta):
		max_age = max_age.total_seconds()

	def get_methods():
		if methods is not None:
			return methods

		options_resp = current_app.make_default_options_response()
		return options_resp.headers['allow']

	def decorator(f):
		def wrapped_function(*args, **kwargs):
			if automatic_options and request.method == 'OPTIONS':
				resp = current_app.make_default_options_response()
			else:
				resp = make_response(f(*args, **kwargs))
			if not attach_to_all and request.method != 'OPTIONS':
				return resp

			h = resp.headers

			h['Access-Control-Allow-Origin'] = origin
			h['Access-Control-Allow-Methods'] = get_methods()
			h['Access-Control-Max-Age'] = str(max_age)
			if headers is not None:
				h['Access-Control-Allow-Headers'] = headers
			return resp

		f.provide_automatic_options = False
		return update_wrapper(wrapped_function, f)
	return decorator


def nocache(view):
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return update_wrapper(no_cache, view)


##############################
# Error Handler
##############################

import traceback

def prompt_admins_for_error(request, status):
	request_ip = request.__dict__['environ']['REMOTE_ADDR']
	request_info = {
		'url': request.url,
		'method': request.method,
		'raw_request_data': unicode(request.__dict__),
		'post_data': unicode(request.get_json()),
		'stacktrace': traceback.format_exc(),
		'request_ip': request_ip,
	}
	html = '<strong>method:</strong> {method}<br><br><strong>request ip:</strong> {req_ip}<br><br><strong>url:</strong> {url}<br><br><strong>raw_request_data:</strong> {raw_request_data}<br><br><strong>post_data:</strong> {post_data}, <br><br><strong>stacktrace:</strong> {stacktrace}'.format(method=request_info['method'], req_ip=request_info['request_ip'], url=request_info['url'], raw_request_data=request_info['raw_request_data'], post_data=request_info['post_data'], stacktrace=request_info['stacktrace'])
	mail_sendgrid(to='perakism1982@yahoo.gr', subject='{status} ERROR {req_url}'.format(status=status, req_url=request_info['url']), sender='noreply@error.monitor.btg', bcc='paul@bieh.net', html=html)


def prompt_admins_for_paylah_error(response, status):
	request_info = {
		'url': response.url,
	}
	html = '<strong>url:</strong> {url}<br><br><strong>stacktrace:</strong> {stacktrace}'.format(url=request_info['url'])
	mail_sendgrid(to='perakism1982@yahoo.gr', subject='{status} PAYLAH SERVER ERROR'.format(status=status), sender='noreply@error.monitor.btg', bcc='paul@bieh.net', html=html)



@app.errorhandler(400)
def missing_data_error(error):
	prompt_admins_for_error(request, 400)
	return "Missing Parameters", 400


@app.errorhandler(500)
def ise_error(error):
	if not request.url.endswith('sidebar.json'):
		prompt_admins_for_error(request, 500)
	return "Internal Server Error", 500


##############################
# Jinja Filters
##############################
@app.template_filter('timestamp2date')
def timestamp2date(s):
	return datetime.datetime.fromtimestamp(s).strftime('%d-%m-%Y T%H:%M:%S')

@app.context_processor
def store_from_order_id():
	def store_from_order_id(app_, oid):
		app_order = string.capwords(app_)+'Order'
		app_order = getattr(models, app_order)

		app_store = string.capwords(app_)+'Store'
		app_store = getattr(models, app_store)
		if not oid:
			return "(unknown " + str(oid) + ")"
		o = app_order.query.get(oid)
		if not o:
			return "(unknown order " + str(oid) + ")"
		s = app_store.query.get(o.store)
		if not s:
			return "(unknown store " + str(o.store) + ")"
		return s.name
	return dict(store_from_order_id=store_from_order_id)


@app.context_processor
def status_from_order_id():
	def status_from_order_id(app_, oid):
		app_order = string.capwords(app_)+'Order'
		app_order = getattr(models, app_order)

		o = app_order.query.get(oid)

		order_status = {
			1: 'received',
			2: 'attending',
			3: 'complete',
			4: 'cancelled',
			5: 'paid',
			6: 'failed',
			7: 'collected'
		}
		return order_status[o.status]
	return dict(status_from_order_id=status_from_order_id)


@app.template_filter('adddecs')
def add_decs2float(s):
	if s is not None:
		return format(float(s), '.2f')


@app.template_filter('timestamp2countdown')
def timestamp2date(s):
	diff = time.time() - s
	if diff < 60:
		return str(int(diff)) + "s ago"

	elif diff < (60*60):
		return str(int(diff/60)) + "m ago"

	elif diff < (60*60*24):
		return str(int(diff/(60*60))) + "h ago"

	else:
		return str(int(diff/(60*60*24))) + "d ago"


@app.template_filter('truncate2link')
def truncate2link(s):
	if s is not None:
		return s[:150]


@app.template_filter('truncate')
def truncate(s):
	if s is not None:
		return s[:10]


@app.template_filter('prettyprint')
def prettyprint(s):
	if s.startswith("<?xml version='1.0' encoding='UTF-8'?>"):
		from lxml import etree
		s = s.replace('&lt;', '<').replace('&#xd;', '').replace('><','>\n<').replace('&gt;', '>').strip()
		s = s[s.find("<ns:return>")+len('<ns:return>\n<?xml version="1.0" encoding="UTF-8"?>'):s.find("</ns:return>")]
		try:
			root = etree.fromstring(s)
			return etree.tostring(root, pretty_print=True)
		except:
			return s
	return s


@app.template_filter('countdown_precise')
def countdown_precise(seconds, granularity=3, suffix=" ago"):

	intervals = (
	('weeks', 604800),  # 60 * 60 * 24 * 7
	('days', 86400),    # 60 * 60 * 24
	('hours', 3600),    # 60 * 60
	('minutes', 60),
	('seconds', 1),
	)

	seconds = time.time() - seconds

	if seconds < 10:
		return "just now"

	result = []

	for name, count in intervals:
		value = seconds // count
		if value:
			seconds -= value * count
			if value == 1:
				name = name.rstrip('s')
			result.append("{} {}".format(int(value), name))
	return ', '.join(result[:granularity]) + suffix


@app.template_filter('order_status_name')
def order_status_name(i):
	order_status = {
		 1: 'received',
		 2: 'attending',
		 3: 'complete',
		 4: 'cancelled',
		 5: 'paid',
		 6: 'failed',
		 7: 'collected'
	}
	return order_status[i]


@app.template_filter('timestr')
def timestr(timestamp, fmt='%Y-%m-%d %H:%M:%S'):
	if not timestamp:
		return "never"
	if timestamp == "now" or int(timestamp) == -1:
		timestamp = time.time()
	return datetime.datetime.fromtimestamp(
		timestamp
	).strftime(fmt)


@app.template_filter('order_details')
def order_details(s):
	details = json.loads(s)
	details_formed = ''
	for item in details:
		details_formed += str(item['quantity'])+' x '+item['menu_item']+', '
	return details_formed.rstrip(', ')

@app.template_filter('payment_status')
def payment_status(status):
	status_map = {0: 'pending',
				  1: 'completed',
				  2: 'failed',
				  3: 'refunded',
				  4: 'cancelled by client'}
	return status_map[status]


@app.template_filter('order_status')
def order_status(status):
	order_status = {
		 1: 'received',
		 2: 'attending',
		 3: 'complete',
		 4: 'cancelled',
		 5: 'paid',
		 6: 'failed',
		 7: 'collected'
	}
	return order_status[status]


@app.template_filter('payment_type')
def payment_type(ptype):
	payment_type = ptype.split(' ')[0]
	return payment_type


@app.template_filter('join_list_or')
def join_list_or(ls):
	ls = json.loads(ls)
	ls = ' or '.join(ls)
	return ls

@app.template_filter('join_list_comma')
def join_list_comma(ls):
	ls = json.loads(ls)
	ls = ', '.join(ls)
	return ls


@app.template_filter('join_list_pipe')
def join_list_pipe(ls):
	ls = json.loads(ls)
	ls = '|'.join(ls)
	return ls


################################################################################
# Send a SMS
################################################################################
def send_sms(to, message):
	ENDPOINT = "https://mx.fortdigital.net/http/send-message"
	smsApiUsername = "*****"
	smsApiPassword = "***********"
	smsApiFromValue = "*********"

	url = ENDPOINT + "?username=" + smsApiUsername + \
	"&password=" + smsApiPassword + \
	"&to=" + to + \
	"&from=" + smsApiFromValue + "&message=" + message;

	data = urllib.urlopen(url).read()
	if "OK" in data:
		return True
	return False


def mail_sendgrid(to, subject, sender, bcc=None, body=None, filepath=None, html=None):
	import sendgrid
	sendgrid_client = sendgrid.SendGridClient(SENDGRID_API_KEY)

	message = sendgrid.Mail()
	message.add_to(to)
	if bcc is not None:
		message.add_bcc(bcc)
	message.set_from(sender)
	message.set_subject(subject)
	message.set_html(html)
	message.set_text(body)
	if filepath:
		filename = filepath.split('/')[-1]
		message.add_attachment(str(filename), str(filepath))

	status, msg = sendgrid_client.send(message)

	if status != 200:
		#print status, msg
		return False

	return status == 200

################################################################################
# Foodfare v2 api
################################################################################

@app.route('/foodfare/get_stalls', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_stalls():
	from datetime import datetime

	filters = request.args.get('filters')
	from_ = request.args.get('from')
	to = request.args.get('to')
	sortby = request.args.get('sortby')
	data = FoodfareStall.query.filter_by(store_id=request.args['storeid'], shown=True)

	if sortby == 'popular':
		data = data.order_by(FoodfareStall.popularity.desc())
	elif sortby == 'a-z':
		data = data.order_by(FoodfareStall.name)
	elif sortby == 'z-a':
		data = data.order_by(FoodfareStall.name.desc())

	stalls = []
	ids = []
	data = data.all()
	if filters:
		for f in filters.split(','):
			for item in data:
				tags = json.loads(item.tags)
				tags = [tag.strip().lower() for tag in tags]
				if f.strip().lower() in tags:
					if item.id not in ids:
						ids.append(item.id)
						stalls.append(item.serialize())
	else:
		stalls = [stall.serialize() for stall in data]

	stalls_timesloted = []
	if from_ and to:
		now = datetime.now()
		from_ = datetime.strptime(from_, '%H:%M').replace(year=now.year, month=now.month, day=now.day)
		to = datetime.strptime(to, '%H:%M').replace(year=now.year, month=now.month, day=now.day)
		for stall in stalls:
			timeslots = stall['timeslots']
			for timeslot in timeslots:
				if timeslot['time'] != 'ASAP' and timeslot['open'] > 0:
					timeslot = datetime.strptime(timeslot['time'], '%H:%M').replace(year=now.year, month=now.month, day=now.day)
					if from_<= timeslot <= to:
						if stall not in stalls_timesloted:
							stalls_timesloted.append(stall)
		stalls = stalls_timesloted

	return api_response(ret=stalls)


@app.route('/foodfare/stall_timeslots', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_stall_timeslots():
	from collections import OrderedDict
	stall = FoodfareStall.query.get(request.args['id'])
	if not stall:
		return api_response(error='Stall does not exist')
	timeslots = json.loads(stall.timeslots, object_pairs_hook=OrderedDict)
	return api_response(ret=timeslots)


@app.route('/foodfare/feedback', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_feedback():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid token or not provided at all")

	params = request.get_json()
	if params is None:
		params = request.form

	feedback = FoodfareFeedback(user.id, unicode(params['body']))
	db.session.add(feedback)
	db.session.commit()

	file = request.files.get('file')
	if file and allowed_file(file.filename):
		filename = file.filename
		fullpath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
		file.save(fullpath)
		feedback.file = request.host_url+'static/uploads/'+filename
		db.session.commit()

	subject = 'Feedback received'
	html = ("User: {user_name}"
			"Feedback:<br>"
			"{feedback}"
			"<br><br><img src='http://foodfare2.butlerpad.com/static/img/foodfare_logo.jpg'"
			"alt='Foodfare logo' width='250'>")
	html = html.format(user_name=user.email, feedback=unicode(feedback.body))

	mail_sent_to_admin = mail_sendgrid(to='support@foodfare.com.sg', subject=subject, sender=user.email, html=html)

	subject = 'Feedback sent successfully'
	html = ("Hi, we have successfully received your feedback! We will reply asap!"
			"</strong><br><br>Thanks, <br><br>-Foodfare<br><br><img src='http://foodfare2.butlerpad.com/static/img/foodfare_logo.jpg'"
			"alt='Foodfare logo' width='250'>")
	mail_sent_to_user = mail_sendgrid(to=user.email, subject=subject, sender="noreply@foodfare.com.sg", html=html)

	if all([mail_sent_to_admin, mail_sent_to_user]):
		return api_response(ret='Emails sent successfully')

	return api_response(error='email not sent')


@app.route('/foodfare/inbox', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_inbox():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	messages = FoodfareMessage.query.filter_by(user_id=user.id).order_by(FoodfareMessage.timestamp.desc()).limit(20).all()
	messages = [message.serialize() for message in messages]

	return api_response(ret=messages)


@app.route('/foodfare/mark_message_read', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_mark_message_read():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	message = FoodfareMessage.query.get(request.args['id'])
	message.unread = False
	db.session.commit()

	messages = FoodfareMessage.query.filter_by(user_id=user.id).order_by(FoodfareMessage.timestamp.desc()).limit(20).all()
	messages = [message.serialize() for message in messages]
	return api_response(ret=messages)


@app.route('/foodfare/send_message', methods=['POST', "OPTIONS"])
@crossdomain(origin='*', headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_send_message():
	params = request.get_json()
	if params is None:
		params = request.form

	user_id = params["user_id"]
	body = params["body"]
	subject = params["subject"]
	message = FoodfareMessage(user_id, subject)
	message.body = body
	db.session.add(message)
	db.session.commit()
	send_push_notification(user_id, subject, message.body)
	return api_response()


@app.route('/foodfare/user_add_favorite', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_add_user_favorites():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	favorites = json.loads(user.favorites)
	favorites.append(int(request.args['itemid']))
	user.favorites = json.dumps(favorites)
	db.session.commit()
	return api_response(ret=user.serialize())


@app.route('/foodfare/user_remove_favorite', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_remove_user_favorites():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	favorites = json.loads(user.favorites)
	favorites.remove(int(request.args['itemid']))
	user.favorites = json.dumps(favorites)
	db.session.commit()
	return api_response(ret=user.serialize())


@app.route('/foodfare/user_get_favorites', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_user_get_favorites():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")
	items = []

	for item_id in json.loads(user.favorites):
		menu_item = FoodfareMenuItem.query.get(item_id)
		if menu_item:
			items.append(menu_item.serialize())

	return api_response(ret=items)


@app.route('/foodfare/remove_wallet', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_wallet_remove_payment_method():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid token or not provided at all")

	payment_method_id = request.args['id']
	wallet = FoodfareWallet.query.filter_by(user_id=user.id, id=payment_method_id).first()
	if wallet is None:
		return api_response(error="Payment method doesn't exist")
	db.session.delete(wallet)
	db.session.commit()

	wallet = FoodfareWallet.query.filter_by(user_id=user.id).all()
	wallet = [pm.serialize() for pm in wallet]
	return api_response(ret=wallet)


@app.route('/foodfare/edit_wallet', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_wallet_edit_payment_method():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid token or not provided at all")

	payment_method_id = request.args['id']
	is_primary = True if request.args['is_primary'] == 'true' else False

	wallet = FoodfareWallet.query.filter_by(user_id=user.id, id=payment_method_id).first()
	if wallet is None:
		return api_response(error="Payment method doesn't exist")
	wallet.is_primary = is_primary
	db.session.commit()

	wallet = FoodfareWallet.query.filter_by(user_id=user.id).all()
	wallet = [pm.serialize() for pm in wallet]
	return api_response(ret=wallet)


@app.route('/foodfare/get_wallet', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_wallet_get_payment_methods():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	wallet = [pm.serialize() for pm in FoodfareWallet.query.filter_by(user_id=user.id).all()]
	return api_response(ret=wallet)


@app.route('/foodfare/calculate_price', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_calculate_price():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	price = float(request.args['amount'])
	promo_code = request.args.get('promo')
	promo_config = FoodfarePromoConfig.query.filter_by(code=promo_code, active=True).first()

	if promo_config is None:
		return api_response(error='Promo code is not active or invalid')

	if promo_code is not None:
		promo_applied_order = FoodfareOrder.query.filter(FoodfareOrder.user_id==user.id, FoodfareOrder.promo_id==promo_config.id, FoodfareOrder.status.in_([2,3,4,5])).first()
		if promo_applied_order is not None:
			return api_response(error="Promo code is used or not valid")
		price -= promo_config.value
	if price <= 0:
		return api_response(error='Invalid final price')
	return api_response(ret=round(price, 2))


@app.route('/foodfare/get_order', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_order():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	order_id = request.args['id']

	order = FoodfareOrder.query.filter_by(id=order_id, user_id=user.id).first()
	if order is None:
		return api_response(error="Order does not exist")
	return api_response(ret=order.serialize())


@app.route('/foodfare/get_order_receipt', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_order_receipt():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	order_id = request.args['id']

	order = FoodfareOrder.query.filter_by(user_id=user.id, id=order_id).first()
	if order is None:
		return api_response(error="Order does not exist")

	payment = FoodfarePayment.query.filter_by(order_id=order.id).first()

	order_receipt = {}
	order_receipt['order_no'] = order.id
	order_receipt['location'] = order.store_obj.name
	order_receipt['date'] = datetime.datetime.fromtimestamp(order.timestamp).strftime('%d %b %Y, %H:%M:%S')
	order_receipt['payment_method'] = 'credit card' if payment.payment_type == 'wirecard' else payment.payment_type
	order_receipt['total'] = order.price
	order_receipt['promo'] = order.promo_obj.value if order.promo_obj else None
	order_receipt['details'] = json.loads(order.details)
	order_receipt['status'] = order.get_status()

	return api_response(ret=order_receipt)


@app.route('/foodfare/make_order', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_make_order():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid token or not provided at all")

	params = request.get_json()
	if params is None:
		params = request.form

	order = FoodfareOrder()

	price = float(params['amount']) #total amount of the order
	details = params['details']
	details = details if isinstance(details, basestring) else json.dumps(details)

	details = json.loads(details)
	tags = []

	for item in details:
		item['status'] = 1
		stall = FoodfareStall.query.get(item['stall_id'])
		timeslots = json.loads(stall.timeslots)
		for timeslot in timeslots:
			if timeslot['time'] == item['collection_time']:
				timeslot['open'] -= item['quantity']
			if timeslot['open'] < 0:
				return api_response(error="Order failed! Menu items amount exceeds available one from food stall {stall_name}. Please try again in a different timeslot!".format(stall_name=stall.name))

		stall.timeslots = json.dumps(timeslots)
		stall.popularity += 1
		db.session.flush()

		menu_item = FoodfareMenuItem.query.filter_by(name=item['menu_item']).first()
		tags.extend(json.loads(menu_item.tags))

	details = json.dumps(details)

	order.user_id = user.id
	order.details = details
	order.comments = params.get('comments')
	order.store_id = params['store_id']
	order.tags = json.dumps(tags)

	promo_code = params.get('promo')
	promo_discount = 0

	if promo_code is not None:
		promo_config = FoodfarePromoConfig.query.filter_by(code=promo_code, active=True).first()

		if promo_config is None:
			return api_response(error='Promo code is not active or invalid')

		promo_applied_order = FoodfareOrder.query.filter(FoodfareOrder.user_id==user.id, FoodfareOrder.promo_id==promo_config.id, FoodfareOrder.status.in_([2,3,5])).first()
		if promo_applied_order is not None:
			return api_response(error="Promo code is used or not valid")

		promo_discount = promo_config.value
		if promo_discount >= price:
			return api_response(error='Your order should be higher than {discount}'.format(discount=promo_discount))

		order.promo_id = promo_config.id

	order.price = price-promo_discount
	db.session.add(order)
	db.session.commit()

	return api_response(ret=order.serialize())


@app.route('/foodfare/update_order', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*', headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_update_order():
	order_id = request.args['orderid']
	order = FoodfareOrder.query.get(order_id)

	params = request.get_json()
	if params is None:
		params = request.form

	for k, v in params.iteritems():
		if k == 'remarks':
			order.remarks = json.loads(order.remarks)
			val_dict = json.loads(v)
			for k, v in val_dict.iteritems():
				order.remarks[k] = v
				order.remarks = json.dumps(order.remarks)
				db.session.commit()
			continue
		setattr(order, k, v)
	db.session.commit()

	return api_response(ret=order.serialize())


@app.route('/foodfare/paylah_payment_create', methods=['GET', "OPTIONS"])
@crossdomain(origin='*', headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_paylah_payment_create():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	return_url = request.args['returnurl']
	order_id = request.args['orderid']
	uat = request.args.get('uat', 'false')

	order = FoodfareOrder.query.get(order_id)
	if order is None:
		return api_response(error='Invalid order id')

	mid = '**************'

	req_params= (
		'/appcheckout?transactionamount={amount}'
		'&returnurl={returnurl}'
		'&postalcode=00000'
		'&mcode={mid}'
	)
	req_params = req_params.format(amount=round(order.price, 2), returnurl=return_url, mid=mid)
	req_payment = requests.get(API_PAYLAH + req_params)
	# evaluates response
	if req_payment.status_code != 200:
		data = {'paylah_status':req_payment.status_code, 'paylah_error_msg': ''}
		prompt_admins_for_paylah_error(req_payment, req_payment.status_code)
		return api_response(error=data)

	data = req_payment.json().get('data')
	data['merchant_code'] = mid
	transaction_status = data.get('txnStatusCode', '666') #the number of the Beast
	if data and transaction_status.startswith('0'):
		success = True
		payment = FoodfarePayment(user.id, order.price, "paylah", False, data['merchantTxnReferenceNumberRes'], data['bankTxnReferenceNumberRes'])
		db.session.add(payment)
		db.session.commit()

		payment.mode = 'uat' if uat == 'true' else 'production'
		payment.order_id = order.id
		db.session.commit()

		data["payment_token"] = payment.payment_token
		paylah_payment_status.apply_async(args=[payment.payment_token])
		return api_response(ret=data)
	else:
		return api_response(error=data)


def wirecard_root(root):
	return root


@app.route('/foodfare/wirecard_payment_create', methods=['GET', "OPTIONS"])
@crossdomain(origin='*', methods=['GET', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_wirecard_payment_create():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	#wirecard urls for various actions
	production = "https://api.wirecard.com.sg/engine/hpp/?"
	sandbox = "https://test.wirecard.com.sg/engine/hpp/?"

	token_production = "https://api.wirecard.com.sg/engine/rest/payments?"
	token_sandbox = "https://test.wirecard.com.sg/engine/rest/payments?"

	#request params
	uat = request.args.get('uat', 'false')
	order_id = request.args['orderid']
	wirecard_token = request.args.get('wirecardToken')
	save = request.args.get('save')

	order = FoodfareOrder.query.get(order_id)
	if order is None:
		return api_response(error='Invalid order id')

	url = token_production if wirecard_token else production
	psp_name = "dbssg"
	auth_pair = {
		'mid': '*******************',
		'secret_key': '*******************'
	}


	if uat == 'true':
		url = token_sandbox if wirecard_token else sandbox
		psp_name = 'test'
		auth_pair = {
			'mid': '******************',
			'secret_key': '******************'
		}
		if wirecard_token:
			auth_pair['mid'] = '******************'
			auth_pair['secret_key'] = '******************'


	#stores payment in our database
	payment_token = str(uuid.uuid4())
	payment = FoodfarePayment(user.id, order.price, 'wirecard', False, payment_token)
	db.session.add(payment)
	db.session.commit()

	payment.mode = 'uat' if uat == 'true' else 'production'
	payment.save = False if save in ['false', None] else True
	payment.order_id = order_id
	db.session.commit()


	auth = {'username':'******************', 'password': '******************'}

	#requesting wirecard api
	success_redirect_url = request.args.get('successRedirect', wirecard_root(request.url_root)+"wirecard_payment/success?ptoken="+payment.get_token())
	fail_redirect_url = request.args.get('failRedirect', wirecard_root(request.url_root)+"wirecard_payment/fail?ptoken="+payment.get_token())
	cancel_redirect_url = request.args.get('cancelRedirect', wirecard_root(request.url_root)+"wirecard_payment/fail?ptoken="+payment.get_token())
	res = ee_payment(amount=payment.amount, currency='SGD', payment_token=payment.payment_token,
					  success_redirect_url=success_redirect_url, fail_redirect_url=fail_redirect_url,
					  cancel_redirect_url=cancel_redirect_url, merchant_account_id=auth_pair['mid'],
					  secret_key=auth_pair['secret_key'], url=url, psp_name=psp_name,
					  wirecard_token=wirecard_token if wirecard_token else '',
					  bypass='true' if wirecard_token else 'false', auth=auth)

	wirecard_payment_status.apply_async(args=[payment.payment_token])

	res = json.loads(res)
	parsed = None
	params = res['params']
	#handles seemless payments
	if params is not None:
		details = json.loads(order.details)

		if params.startswith('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'):
			parsed = xmltodict.parse(params)
			parsed = json.dumps(parsed, indent=4)
			parsed = json.loads(parsed)
		else:
			parsed = urlparse.urlsplit(params).path
			parsed = dict(urlparse.parse_qsl(parsed))

		txn_state = parsed.get('transaction_state')

		#non-uniform response handler
		if txn_state and txn_state != 'success':
			payment.status = 2
			if order.status == 1:
				order.status = 6
				for item in details:
					if item['status'] == 1:
						item['status'] = 6
						stall = FoodfareStall.query.get(item['stall_id'])
						timeslots = json.loads(stall.timeslots)
						for timeslot in timeslots:
							if timeslot['time'] == item['collection_time']:
								timeslot['open'] += item['quantity']
							if timeslot['open'] > timeslot['slots']:
								timeslot['open'] = timeslot['slots']
						stall.timeslots = json.dumps(timeslots)
			order.details = json.dumps(details)
			db.session.commit()
			return api_response(error=parsed['status_description_1'])
		elif not txn_state :
			txn_state = parsed['payment']['transaction-state']
			if txn_state == 'failed':
				payment.status = 2
				if order.status == 1:
					order.status = 6
					for item in details:
						if item['status'] == 1:
							item['status'] = 6
							stall = FoodfareStall.query.get(item['stall_id'])
							timeslots = json.loads(stall.timeslots)
							for timeslot in timeslots:
								if timeslot['time'] == item['collection_time']:
									timeslot['open'] += item['quantity']
								if timeslot['open'] > timeslot['slots']:
									timeslot['open'] = timeslot['slots']
							stall.timeslots = json.dumps(timeslots)
				order.details = json.dumps(details)
				db.session.commit()
				error_descr = parsed['payment']['statuses']['status']['@description']
				return api_response(error=error_descr)

		payment.status = 1
		payment.bypass = True

		if order.status == 1:
			order.status = 5
			for item in details:
				if item['status'] == 1:
					item['status'] = 5
		order.details = json.dumps(details)
		payment.transaction_id = parsed['transaction_id']
		db.session.commit()

		email = order.user_obj.email
		subject = 'Order #{orderid} payment success'.format(orderid=order.id)
		html = unicode(payment_receipt(order.id))
		mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)

	data = {
		"payment_token": payment.get_token(),
		"amount": payment.amount,
		"currency": payment.currency,
		"redirect_url": res['redirect_url'],
		"data": parsed
	}
	return api_response(ret=data)


@app.route('/foodfare/wirecard_payment_status', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*', headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_wirecard_payment_status():
	from requests.auth import HTTPBasicAuth
	payment_token = request.args['ptoken']
        payment = FoodfarePayment.query.filter_by(payment_token=payment_token).first()
	auth = {'username':'******************', 'password': '******************'}
	headers = {'Accept': 'application/json'}
	authorization = HTTPBasicAuth(auth['username'], auth['password'])

	mid = '******************'

	url = "https://api.wirecard.com.sg"
	url += "/engine/rest/merchants/{merchant_account_id}/payments/?request_id={request_id}"
	url = url.format(merchant_account_id=mid, request_id=payment_token)

	req = requests.get(url=url, auth=authorization, headers=headers)
	#app.logger.info(req.content)
	print req.content
	data = json.loads(req.content)


	order = None
	if payment is not None:
		if payment.order_id:
			order = FoodfareOrder.query.get(payment.order_id)
		if 'payment' in data['payments']:
			status = data['payments']['payment'][0]['transaction-state']
			if status == 'success':
				if order and order.status == 1:
					order.status = 5
					details = json.loads(order.details)
					for item in details:
						if item['status'] == 1:
							item['status'] = 5
					order.details = json.dumps(details)
					db.session.commit()
				transaction_id = data['payments']['payment'][0]['transaction-id']
				payment.transaction_id = transaction_id
				if payment.status == 0:
					payment.status=1
				db.session.commit()
			elif status == 'failed':
				if order and order.status == 1:
					order.status = 6
					details = json.loads(order.details)
					for item in details:
						if item['status'] == 1:
							item['status'] = 6
							stall = FoodfareStall.query.get(item['stall_id'])
							timeslots = json.loads(stall.timeslots)
							for timeslot in timeslots:
								if timeslot['time'] == item['collection_time']:
									timeslot['open'] += item['quantity']
								if timeslot['open'] > timeslot['slots']:
									timeslot['open'] = timeslot['slots']
							stall.timeslots = json.dumps(timeslots)
					order.details = json.dumps(details)
					db.session.commit()

				transaction_id = data['payments']['payment'][0]['transaction-id']
				payment.transaction_id = transaction_id
				if payment.status == 0:
					payment.status= 2
				db.session.commit()
	db.session.close()
	return api_response(ret=data)


@app.route('/wirecard_token', methods=['GET', 'POST', "OPTIONS"])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_wirecard_token():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	#wirecard urls for various actions
	production = "https://api.wirecard.com.sg/engine/hpp/?"
	sandbox = "https://test.wirecard.com.sg/engine/hpp/?"

	#request params
	uat = request.args.get('uat', 'false')
	redirect_url = request.args.get('redirecturl', "")

	amount = 1.0

	url = production
	psp_name = "dbssg" #"DBSSGNOTOTAL"
	auth_pair = {
		'mid': '******************',
		'secret_key': '******************'
	}
	if uat == 'true':
		url = sandbox
		psp_name = 'test'
		auth_pair = {
			'mid': '******************',
			'secret_key': '******************'
		}

	#stores payment in our database
	payment_token = str(uuid.uuid4())
	payment = FoodfarePayment(user.id, amount, 'wirecard', True, payment_token)
	db.session.add(payment)
	db.session.commit()

	payment.save = True
	payment.mode = 'uat' if uat=='true' else 'production'
	db.session.commit()

	auth = {'username':'******************', 'password': '******************'}

	#requesting wirecard api
	success_redirect_url = request.args.get('successRedirect', wirecard_root(request.url_root)+"wirecard_payment/success?ptoken="+payment.get_token())
	fail_redirect_url = request.args.get('failRedirect', wirecard_root(request.url_root)+"wirecard_payment/fail?ptoken="+payment.get_token())
	cancel_redirect_url = request.args.get('cancelRedirect', wirecard_root(request.url_root)+"wirecard_payment/fail?ptoken="+payment.get_token())
	res = ee_payment(amount=amount, currency='SGD', payment_token=payment.payment_token,
					  success_redirect_url=success_redirect_url, fail_redirect_url=fail_redirect_url,
					  cancel_redirect_url=cancel_redirect_url, merchant_account_id=auth_pair['mid'],
					  secret_key=auth_pair['secret_key'], url=url, psp_name=psp_name,
					  wirecard_token='', bypass='false', auth=auth)

	res = json.loads(res)

	data = {
		"payment_token": payment.get_token(),
		"amount": payment.amount,
		"currency": payment.currency,
		"redirect_url": res['redirect_url']
	}
	return api_response(ret=data)


@app.route('/foodfare/paylah_payment_refund', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_paylah_payment_refund():

	return_url = request.args.get('returnurl', '')
	payment_token = request.args['ptoken']
	payment = FoodfarePayment.query.filter_by(payment_token=payment_token).first()
	if payment is None:
		return api_response(error="Payment does not exist")
	return void_paylah_txn(return_url, payment)



@app.route('/foodfare/get_orders', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_orders():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	orders = [o.serialize()
		for o in FoodfareOrder.query.filter(FoodfareOrder.user_id==user.id, FoodfareOrder.status!=1)
		.order_by(FoodfareOrder.timestamp.desc()).limit(100).all()]

	return api_response(ret=orders)


@app.route('/foodfare/get_orders_all', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def ajax_get_orders_all():

	#orders_data = FoodfareOrder.query.filter(FoodfareOrder.status != 1).filter(FoodfareOrder.status != 6).filter(FoodfareOrder.status != 4)
	orders_data = FoodfareOrder.query
	recordsTotal = orders_data.count()
	recordsFiltered = recordsTotal

	limit = int(request.args['length'])
	draw = int(request.args['draw'])
	start = int(request.args['start'])
	search = request.args['search[value]']

	fromdate = request.args['columns[1][search][value]']
	todate = request.args['columns[2][search][value]']
	storeids = request.args['columns[3][search][value]']

	if search != '':
		orders_data = orders_data.filter(or_(FoodfareOrder.details.ilike("%"+search+"%"), FoodfareOrder.user_id.ilike("%"+search+"%"), FoodfareOrder.id.ilike("%"+search+"%"), FoodfareOrder.tags.ilike("%"+search+"%")))
		recordsFiltered = orders_data.count()

	if fromdate != '':
		fromdate = time.mktime(datetime.datetime.strptime(fromdate, "%Y-%m-%d").timetuple())
		orders_data = orders_data.filter(FoodfareOrder.timestamp >= fromdate)
		recordsFiltered = orders_data.count()

	if todate != '':
		todate = datetime.datetime.strptime(todate, "%Y-%m-%d") + datetime.timedelta(days=1)
		todate = time.mktime(todate.timetuple())
		orders_data = orders_data.filter(FoodfareOrder.timestamp < todate)
		recordsFiltered = orders_data.count()

	if storeids != '':
		storeids = map(int, storeids.split(','))
		orders_data = orders_data.filter(FoodfareOrder.store_id.in_(storeids))
		recordsFiltered = orders_data.count()

	orders_data = orders_data.order_by(FoodfareOrder.timestamp.desc())

	if limit != -1:
		orders_data = orders_data.slice(start, start+limit)

	orders = []
	for order in orders_data.all():
		os = order.serialize()
		sn = ""
		for i in os["details"]:
			sn += i["stall_name"] + ", "
		sn = sn.strip()[:-1]
		order_ls = [
			order.id, order.store_obj.name,
			sn,
			"$%0.2f" % (order.price,), order.promo_obj.code if order.promo_obj else None,
			order.get_status(), order.user_obj.id,
			datetime.datetime.fromtimestamp(order.timestamp).strftime('%Y-%m-%d %H:%M:%S')
		]
		order = os
		details_formed = ''
		for item in order['details']:
			details_formed += str(item['quantity'])+' x '+item['menu_item']+' (' + ("$%0.2f" % (item['price'],)) + ')<br>'
		order['details'] = details_formed.rstrip(', ')
		order_ls.append(order['details'])
		orders.append(order_ls)

	return jsonify(data=orders, draw=draw, recordsTotal=recordsTotal, recordsFiltered=recordsFiltered)


@app.route('/foodfare/get_feedback_all', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def ajax_get_feedback_all():

	data = FoodfareFeedback.query.order_by(FoodfareFeedback.timestamp.desc())
	recordsTotal = data.count()
	recordsFiltered = recordsTotal

	search = request.args['search[value]']
	if search != '':
		data = FoodfareFeedback.query.filter(or_(FoodfareFeedback.body.ilike("%"+search+"%"), FoodfareFeedback.user.ilike("%"+search+"%"))).order_by(FoodfareFeedback.timestamp.desc())
		recordsFiltered = data.count()

	limit = int(request.args['length'])
	draw = int(request.args['draw'])
	start = int(request.args['start'])


	if limit != -1:
		data = data.slice(start, start+limit)

	feedback = []
	for f in data.all():
		f_ls = [
			f.id, f.body,
			f.user, datetime.datetime.fromtimestamp(f.timestamp).strftime('%Y-%m-%d %H:%M:%S'),
			f.file
		]
		feedback.append(f_ls)

	return jsonify(data=feedback, draw=draw, recordsTotal=recordsTotal, recordsFiltered=recordsFiltered)


@app.route('/foodfare/get_payments_all', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def ajax_get_payments_all():
	order_status = {
		 1: 'received',
		 2: 'attending',
		 3: 'complete',
		 4: 'cancelled',
		 5: 'paid',
		 6: 'failed',
		 7: 'collected'
	}

	payments_data = FoodfarePayment.query.filter(FoodfarePayment.order_id != None, FoodfarePayment.status == 1, FoodfarePayment.mode=='production')
	recordsTotal = payments_data.count()
	recordsFiltered = recordsTotal

	limit = int(request.args['length'])
	draw = int(request.args['draw'])
	start = int(request.args['start'])
	search = request.args['search[value]']

	fromdate = request.args['columns[1][search][value]']
	todate = request.args['columns[2][search][value]']
	storeids = request.args['columns[3][search][value]']

	if search != '':
		payments_data = payments_data.filter(or_(FoodfarePayment.order_id.ilike("%"+search+"%"), FoodfarePayment.payment_type.ilike("%"+search+"%"), FoodfarePayment.user_id.ilike("%"+search+"%")))
		recordsFiltered = payments_data.count()

	if fromdate != '':
		fromdate = time.mktime(datetime.datetime.strptime(fromdate, "%Y-%m-%d").timetuple())
		payments_data = payments_data.filter(FoodfarePayment.timestamp >= fromdate)
		recordsFiltered = payments_data.count()

	if todate != '':
		todate = datetime.datetime.strptime(todate, "%Y-%m-%d") + datetime.timedelta(days=1)
		todate = time.mktime(todate.timetuple())
		payments_data = payments_data.filter(FoodfarePayment.timestamp < todate)
		recordsFiltered = payments_data.count()


	payments_data = payments_data.order_by(FoodfarePayment.timestamp.desc())
	payments_raw = []
	if storeids != '':
		storeids = map(int, storeids.split(','))
		for payment in payments_data:
			oid = payment.order_id
			order = FoodfareOrder.query.get(oid)
			if order:
				if storeids:
					if order.store_id in storeids:
						payments_raw.append(payment)
		recordsFiltered = len(payments_raw)
		if limit != -1:
			payments_raw = payments_raw[start:start+limit]
	else:
		if limit != -1:
			payments_data = payments_data.slice(start, start+limit).all()
		payments_raw = payments_data

	payments = []

	for payment in payments_raw:
		oid = payment.order_id
		order = FoodfareOrder.query.get(oid)
		if order:
			order_details = json.loads(order.details)

			sn = ""
			for i in order_details:
				sn += i["stall_name"] + ", "
			sn = sn.strip()[:-1]

			payment_ls = [order.id, order.get_status(), order.store_obj.name,
				sn,
				"$%0.2f" % (payment.amount,), payment.user_id, payment.id,
				datetime.datetime.fromtimestamp(payment.timestamp).strftime('%Y-%m-%d %H:%M:%S'),
				payment.payment_type, payment.payment_token]
			payments.append(payment_ls)

	return jsonify(data=payments, draw=draw, recordsTotal=recordsTotal, recordsFiltered=recordsFiltered)

@app.route('/foodfare/get_users_all', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def ajax_get_users_all():

	limit = int(request.args['length'])
	draw = int(request.args['draw'])
	start = int(request.args['start'])
	search = request.args['search[value]']

	data = FoodfareUser.query.filter(~FoodfareUser.email.ilike('%test%')).filter(~FoodfareUser.email.ilike('%undefined%'))
	recordsTotal = data.count()
	recordsFiltered = recordsTotal

	if search != '':
		#data = data.filter(or_(FoodfareUser.nric.ilike("%"+search+"%"), we are FoodfareUser.id.ilike("%"+search+"%"), FoodfareUser.email.ilike("%"+search+"%")))
		data = data.filter(or_(FoodfareUser.id.ilike("%"+search+"%"), FoodfareUser.email.ilike("%"+search+"%")))
		recordsFiltered = data.count()

	data = data.order_by(FoodfareUser.id.desc())
	if limit != -1:
		data = data.slice(start, start+limit)

	users = []
	for user in data.all():
		user = user.serialize()
		user['payments'] = FoodfarePayment.query.filter(FoodfarePayment.user_id==user['id'], FoodfarePayment.status==1).count()
		user_ls = [user['id'], user['email'], user['token'], user['payments']]
		users.append(user_ls)

	return jsonify(data=users, draw=draw, recordsTotal=recordsTotal, recordsFiltered=recordsFiltered)


@app.route('/foodfare/stall_order/cancelled/<oid>', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*', headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_stall_cancel_order(oid):
	order = FoodfareOrder.query.get(oid)
	if order.status == 4:
		return api_response(error='Order has already been cancelled once!')

	payment = FoodfarePayment.query.filter_by(order_id=order.id).first()
	if payment:
		req = None
		if payment.payment_type == "wirecard":
			req = void_wirecard_txn(payment)
		elif payment.payment_type == "paylah":
			req = void_paylah_txn('', payment)
		if req:
			if req.get_json()['success'] is False:
				return api_response(error="Payment refund failed")

	send_push_notification(order.user_obj.id, "Order #{order_id} cancelled.".format(order_id=order.id), 'Unfortunately we are not able to accept your order at this moment. Please choose another time slot or visit us at our stall.')

	return api_response(ret=order.serialize())


@app.route('/foodfare/stall_order/<action>/<oid>', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*', headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_stall_order_action(action, oid):
	order = FoodfareOrder.query.get(oid)
	order.unread = False
	order.unread_by_user = True
	order.timestamp = int(time.time())

	stall_id = int(request.args['stallid'])
	message = None
	details = json.loads(order.details)

	status = 1
	if action == 'confirmed':
		status = 2
		message = 'Thank you for your order, we will now prepare your {item}.'
	elif action == 'complete':
		status = 3
		message = 'Your {item} is now ready for collection. Please proceed to your selected stall.'
		for item in details:
			stall = FoodfareStall.query.get(stall_id)
			timeslots = json.loads(stall.timeslots)
			for timeslot in timeslots:
				if timeslot['time'] == item['collection_time']:
					if timeslot['open'] < timeslot['slots']:
						timeslot['open'] += item['quantity']
				if timeslot['open'] > timeslot['slots']:
					timeslot['open'] = timeslot['slots']
			stall.timeslots = json.dumps(timeslots)
			db.session.commit()
	elif action == 'collected':
		status = 7
		message = 'Thank you for your purchase. Enjoy your {item}.'

	for item in details:
		if item['stall_id'] == stall_id:
			item['status'] = status
			send_push_notification(order.user_obj.id, "Order #{order_id} {item} {action}.".format(order_id=order.id, item=str(item['quantity'])+'x'+item['menu_item'],  action=action), message.format(item=item['menu_item']))

	order.details = json.dumps(details)
	db.session.commit()

	order.status = status if all(item['status'] == status for item in details) else order.status
	db.session.commit()

	return api_response(ret=order.serialize())


@app.route('/foodfare/get_stall', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_stall():
	pin = request.args['pin']
	stall = FoodfareStall.query.filter_by(pin=pin).first()
	if stall is None:
		return api_response(error='Invalid pin')

	stall = stall.serialize()
	return api_response(ret=stall)


@app.route('/foodfare/get_stores', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_stores():
	stores = FoodfareStore.query.filter_by(status=1).all()
	stores = [store.serialize() for store in stores]
	return api_response(ret=stores)


@app.route('/foodfare/update_store', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_update_store():
	store = FoodfareStore.query.get(request.args['id'])
	if store is None:
		return api_response(error='Store does not exist')
	for k, v in request.form.iteritems():

		if k == 'disabled':
			if v == 'true':
				v = True
			else:
				v = False

		if k == 'status':
			v = 1 if v == '1' else 0

		setattr(store, k, v)
	db.session.commit()

	return api_response(ret=store.serialize())


@app.route('/foodfare/update_stall', methods=['GET', 'POST', "OPTIONS"])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_update_stall():
	stall = FoodfareStall.query.get(request.args['id'])
	if stall is None:
		return api_response(error='Store does not exist')
	for k, v in request.form.iteritems():
		if v == 'true':
			v = True
		elif v == 'false':
			v = False
		setattr(stall, k, v)
	db.session.commit()
	return api_response(ret=stall.serialize())


@app.route('/foodfare/update_promo_config', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_update_promo():
	pcid = request.args['id']
	promo_config = FoodfarePromoConfig.query.get(pcid)
	status = request.form['status']
	promo_config.active = True if status == '1' else False
	db.session.commit()
	return api_response(ret=promo_config.serialize())


@app.route('/foodfare/update_ad', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_update_ad():
	adid = request.args['id']
	ad = FoodfareAd.query.get(adid)
	status = request.form['status']
	ad.active = True if status == '1' else False
	db.session.commit()
	return api_response(ret=ad.serialize())


@app.route('/foodfare/stall_get_order', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_stall_get_order():

	order_status = {
		 1: 'received',
		 2: 'attending',
		 3: 'complete',
		 4: 'cancelled',
		 5: 'paid',
		 6: 'failed',
		 7: 'collected'
	}
	order_id = request.args['orderid']
	stall_id = request.args['stallid']

	order = FoodfareOrder.query.get(order_id)
	if order is None:
		return api_response(ret={})

	user_id = order.user_obj.id
	user_phone = order.user_obj.phone
	order.unread = False
	db.session.commit()

	order = order.serialize()
	order['user'] = user_id
	order['phone'] = user_phone
	n_details = ''
	order_details = order['details']
	amount = 0
	take_away = 0
	for i in order_details:
		if i['stall_id'] == int(stall_id):
			n_details += str(i['quantity'])+' x '+i['menu_item']
			modifiers = i['submenu']+i['addons']+i['selections']
			for item in modifiers:
				n_details += ' (' +item['name']+' x '+ str(item['quantity']) + ')'
				if item['price']:
					amount += float(item['price'])*item['quantity']

			if i['special_request']:
				n_details += ' special request: '+i['special_request']

			n_details += ', <br>'
			amount += i['price']*i['quantity']
			if i['take_away']:
				take_away += float(i['take_away'])#*i['quantity']
			order['status'] = order_status[i['status']]
			order['collection_time'] = i['collection_time']

	n_details = n_details.rstrip(', <br>')
	order['details'] = n_details
	order['price'] = format(amount+take_away, '.2f')
	return api_response(ret=order)


@app.route('/foodfare/stall_orders', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_stall_orders():

	order_status = {
		 1: 'received',
		 2: 'attending',
		 3: 'complete',
		 4: 'cancelled',
		 5: 'paid',
		 6: 'failed',
		 7: 'collected'
	}
	num_days = int(request.args['days'])
	start_date = datetime.datetime.now() - datetime.timedelta(days=num_days)
	start_date = int(start_date.strftime('%Y%m%d'))
	orders = FoodfareOrder.query.order_by(FoodfareOrder.timestamp.desc()).all()
	normalized_orders =[]
	for o in orders:
		if o.status not in [1, 6]:
			order = o.serialize()
			details = order['details']
			order_date = int(datetime.datetime.fromtimestamp(order['timestamp']).strftime('%Y%m%d'))
			if start_date <= order_date:
				amount = 0
				take_away = 0
				n_details = ''
				for i in details:
					if i['stall_id'] == int(request.args['id']):
						n_details += str(i['quantity'])+' x '+i['menu_item']
						n_details += ', '
						amount += i['price'] * i['quantity']
						order['status'] = order_status[i['status']]

						modifiers = i['submenu']+i['addons']+i['selections']
						for item in modifiers:
							if item['price']:
								amount += float(item['price'])*item['quantity']

						if i['take_away']:
							take_away += float(i['take_away'])#*i['quantity']

				n_details = n_details.rstrip(', ')
				order['details'] = n_details
				order['price'] = format(amount+take_away, '.2f')
				if n_details:
					normalized_orders.append(order)
	return api_response(ret=normalized_orders)


@app.route('/foodfare/orders_not_in_pos', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_orders_not_in_pos():

	order_status = {
		 1: 'received',
		 2: 'attending',
		 3: 'complete',
		 4: 'cancelled',
		 5: 'paid',
		 6: 'failed',
		 7: 'collected'
	}
	orders = FoodfareOrder.query.order_by(FoodfareOrder.timestamp.desc()).filter_by(in_pos=0).all()
	normalized_orders =[]
	for o in orders:
		if o.status not in [1, 6]:
			order = o.serialize()

			details = order['details']
			order_date = int(datetime.datetime.fromtimestamp(order['timestamp']).strftime('%Y%m%d'))
			amount = 0
			take_away = 0
			n_details = ''
			for i in details:
				if "stall" not in request.args or i['stall_id'] == int(request.args.get('stall')):
					n_details += str(i['quantity'])+' x '+i['menu_item']
					n_details += ', '
					amount += i['price'] * i['quantity']
					order['status'] = order_status[i['status']]

					modifiers = i['submenu']+i['addons']+i['selections']
					for item in modifiers:
						if item['price']:
							amount += float(item['price'])*item['quantity']

					if i['take_away']:
						take_away += float(i['take_away'])#*i['quantity']

			n_details = n_details.rstrip(', ')
			order['item_string'] = n_details
			order['price'] = format(amount+take_away, '.2f')

			op = FoodfarePayment.query.filter_by(order_id=o.id).first()

			if op:
				order["payment"] = op.serialize()
			else:
				order["payment"] = None

			if n_details:
				normalized_orders.append(order)

			if request.args.get("test") == "false":
				o.in_pos = 1

	db.session.commit()

	return api_response(ret=normalized_orders)




@app.route('/foodfare/get_menu', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_get_menu():
	filters = request.args.get('filters')
	data = FoodfareMenuItem.query.filter_by(stall_id=int(request.args['stallid']), shown=True).all()
	data = [item.serialize() for item in data]
	menu = []
	ids = []
	if filters:
		for f in filters.split(','):
			for item in data:
				if f in item['tags']:
					if item['id'] not in ids:
						ids.append(item['id'])
						menu.append(item)
	else:
		menu = data
	return api_response(ret=menu)


def void_wirecard_txn(payment):
	from requests.auth import HTTPBasicAuth

	order = FoodfareOrder.query.get(payment.order_id) if payment.order_id else None

	auth = {'username':'******************', 'password': '******************'}
	mid = '******************'
	url = "https://api.wirecard.com.sg/engine/rest/payments?"
	uat = True if payment.mode == 'uat' else False
	bypass = payment.bypass

	if uat is True:
		auth = {'username':'******************', 'password': '******************'} #uat
		mid = '******************' if bypass is False else '******************' #uat
		url = "https://test.wirecard.com.sg/engine/rest/payments?" #uat

	data = ('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><payment xmlns="http://www.elastic-payments.com/schema/payment">'
			'<merchant-account-id>'+mid+'</merchant-account-id>'
			'<request-id>'+str(uuid.uuid4())+'</request-id>'
			'<transaction-type>void-purchase</transaction-type>'
			'<parent-transaction-id>'+payment.transaction_id+'</parent-transaction-id>'
			'<ip-address>127.0.0.1</ip-address></payment>')
	authorization = HTTPBasicAuth(auth['username'], auth['password'])
	headers = {'Content-Type': 'application/xml'}

	void_req = requests.post(url=url, data=data, headers=headers, auth=authorization)
	data = void_req.content
	xmldict = xmltodict.parse(data)
	xmldict = json.dumps(xmldict, indent=4)
	xmldict = json.loads(xmldict)
	if xmldict['payment']['transaction-state'] == 'success':

		#refunds payment
		payment.status = 3
		payment.void = True
		if order:
			#cancels order
			order.status = 4
			order.unread = False
			order.unread_by_user = True
			order.timestamp = int(time.time())

			details = json.loads(order.details)
			for item in details:
				item['status'] = 4
				stall = FoodfareStall.query.get(item['stall_id'])
				timeslots = json.loads(stall.timeslots)
				for timeslot in timeslots:
					if timeslot['time'] == item['collection_time']:
						timeslot['open'] += item['quantity']
					if timeslot['open'] > timeslot['slots']:
						timeslot['open'] = timeslot['slots']
				stall.timeslots = json.dumps(timeslots)

			order.details = json.dumps(details)
			db.session.commit()

			#sends email to costumer
			email = order.user_obj.email
			subject = 'Order #{orderid} payment cancelled'.format(orderid=order.id)
			html = unicode(payment_receipt(order.id))
			mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)

		db.session.commit()
	else:
		error_msg = xmldict['payment']['statuses']['status']['@description']
		#app.logger.info(xmldict)
		print xmldict
		return api_response(error=error_msg)

	return api_response(ret=xmldict)


def void_paylah_txn(return_url, payment):

	mid = '******************'
	return_url = return_url
	order = None

	order = FoodfareOrder.query.get(payment.order_id)
	user = FoodfareUser.query.get(payment.user_id)
	phone_no = '00000'
	if user is not None:
		phone_no = user.phone

	req_params = (
		'/refund?txrefno={txrefno}'
		'&transactionamount={transactionamount}'
		'&returnurl={returnurl}'
		'&phoneno={phoneno}'
		'&dbstxrefno={dbstxrefno}'
		'&mcode={mcode}'
	)
	req_params = req_params.format(txrefno=payment.payment_token,
		transactionamount=payment.amount,
		returnurl=return_url,
		phoneno=phone_no,
		dbstxrefno=payment.transaction_id,
		mcode=mid)
	req_payment = requests.get(API_PAYLAH + req_params)
	if req_payment.status_code != 200:
		data = {'paylah_status':req_payment.status_code, 'paylah_error_msg': ''}
		prompt_admins_for_paylah_error(req_payment, req_payment.status_code)
		return api_response(error=data)

	resp = req_payment.json()
	data = resp["data"]
	data.update({
		'txrefno': payment.payment_token,
		'dbstxrefno': payment.transaction_id,
		'merchant_code': mid,
		'status': resp["status"]
		})

	txn_status = data.get("txnStatusCode", '9999')
	data['success'] = False
	if txn_status =='0000': #checks if transaction is successful
		data['success'] = True #defines success attr to easily handle response afterwards

		#refunds payment
		payment.status = 3
		payment.void = True

		if order:
			#cancels order
			order.status = 4
			order.unread = False
			order.unread_by_user = True
			order.timestamp = int(time.time())

			details = json.loads(order.details)
			for item in details:
				item['status'] = 4
				stall = FoodfareStall.query.get(item['stall_id'])
				timeslots = json.loads(stall.timeslots)
				for timeslot in timeslots:
					if timeslot['time'] == item['collection_time']:
						timeslot['open'] += item['quantity']
					if timeslot['open'] > timeslot['slots']:
						timeslot['open'] = timeslot['slots']
				stall.timeslots = json.dumps(timeslots)

			order.details = json.dumps(details)
			db.session.commit()

			#sends email to costumer
			email = order.user_obj.email
			subject = 'Order #{orderid} payment cancelled'.format(orderid=order.id)
			html = unicode(payment_receipt(order.id))
			mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)

		db.session.commit()
	return api_response(ret=data)


@app.route('/foodfare/wirecard_payment_refund', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_wirecard_payment_refund():
	#request params
	payment_token = request.args.get('ptoken')
	#uat = False

	payment = FoodfarePayment.query.filter_by(payment_token=payment_token).first()
	if payment is None:
		return api_response(error='Invalid payment token')

	return void_wirecard_txn(payment)


@app.route('/foodfare/receipt', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_payment_receipt():
	order_id = request.args['orderid']
	return payment_receipt(order_id)


def payment_receipt(order_id):
	order = FoodfareOrder.query.get(order_id)
	order_details = json.loads(order.details)
	fail_msg = None

	for item in order_details:
		menu_item = item['menu_item']

		if item['submenu']:
			menu_item += '<br>Submenu: '
			for i in item['submenu']:
				price = ' (+'+str(format(i['quantity']*float(i['price']), '.2f'))+'$), ' if i['price'] else ', '
				menu_item += i['name']+' x '+str(i['quantity'])+price
		if item['addons']:
			menu_item += '<br>Add-ons: '
			for i in item['addons']:
				price = ' (+'+str(format(i['quantity']*float(i['price']), '.2f'))+'$), ' if i['price'] else ', '
				menu_item += i['name']+' x '+str(i['quantity'])+price
		if item['selections']:
			menu_item += '<br>Selections: '
			for i in item['selections']:
				price = ' (+'+str(format(i['quantity']*float(i['price']), '.2f'))+'$), ' if i['price'] else ', '
				menu_item += i['name']+' x '+str(i['quantity'])+price

		item['menu_item'] = menu_item.strip(', ')

	payment = FoodfarePayment.query.filter_by(order_id=order.id, status=1).first()
	if payment is None:
		fail_msg = 'Order #{orderid} is cancelled. Incomplete payment for this order.'.format(orderid=order.id)
	user = order.user_obj
	gst = round(order.price/1.07*0.07, 2)
	com_addr = {
		'name': 'NTUC FOODFARE CO-OPERATIVE LTD',
		'num': 'GST Registration No: M400065306',
		'tel': '6550 6500', 'fax': '6752 8411',
		'website': 'http://www.foodfare.com.sg',
		'address': '10 Senoko Way, Singapore 758031'
	}
	return render_template("payment_receipt.html", user=user, order=order,
		item_list=order_details, com_addr=com_addr, payment=payment,
		gst=gst, fail_msg=fail_msg)


@app.route('/foodfare/register_user', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_register_user():
	params = request.get_json()
	if params is None:
		params = request.form

	email = params['email']
	existing_user = FoodfareUser.query.filter_by(email=email).first()
	if existing_user:
		return api_response(error="User already exists")

	user = FoodfareUser()
	for k, v in params.iteritems():
		if v == 'true':
			v = True
		elif v == 'false':
			v = False
		setattr(user, k, v)
	db.session.add(user)
	db.session.commit()

	subject = 'Welcome to Foodfare app'
	html = ("Hi, <br><br>Welcome to Foodfare! You can log into the app with:<br><br><strong>{pwd}"
			"</strong><br><br>Thanks, <br><br>-Foodfare<br><br><img src='http://foodfare2.butlerpad.com/static/img/foodfare_logo.jpg'"
			"alt='Foodfare logo' width='250'>")
	html = html.format(pwd=user.password)
	mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)

	return api_response(ret=user.serialize())


@app.route('/foodfare/facebook_connect', methods=['POST'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_facebook_connect():
	params = request.get_json()
	if params is None:
		params = request.form

	email = params['email']
	existing_user = FoodfareUser.query.filter_by(email=email).first()
	if existing_user:
		existing_user.token = uuid.uuid4()
		db.session.commit()
		return api_response(ret=existing_user.serialize())

	user = FoodfareUser()
	for k, v in params.iteritems():
		if v == 'true':
			v = True
		elif v == 'false':
			v = False
		setattr(user, k, v)
	db.session.add(user)
	db.session.commit()

	subject = 'Welcome to Foodfare app'
	html = ("Hi, <br><br>Welcome to Foodfare! You can log into the app your Facebook account"
			"</strong><br><br>Thanks, <br><br>-Foodfare<br><br><img src='http://foodfare2.butlerpad.com/static/img/foodfare_logo.jpg'"
			"alt='Foodfare logo' width='250'>")
	mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)

	return api_response(ret=user.serialize())


@app.route('/foodfare/google_connect', methods=['POST'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_google_connect():
	params = request.get_json()
	if params is None:
		params = request.form

	email = params['email']
	existing_user = FoodfareUser.query.filter_by(email=email).first()
	if existing_user:
		existing_user.token = uuid.uuid4()
		db.session.commit()
		return api_response(ret=existing_user.serialize())

	user = FoodfareUser()
	for k, v in params.iteritems():
		if v == 'true':
			v = True
		elif v == 'false':
			v = False
		setattr(user, k, v)
	db.session.add(user)
	db.session.commit()

	subject = 'Welcome to Foodfare app'
	html = ("Hi, <br><br>Welcome to Foodfare! You can log into the app your Google account"
			"</strong><br><br>Thanks, <br><br>-Foodfare<br><br><img src='http://foodfare2.butlerpad.com/static/img/foodfare_logo.jpg'"
			"alt='Foodfare logo' width='250'>")
	mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)

	return api_response(ret=user.serialize())


@app.route('/foodfare/user_add_push_tokens', methods=['POST', 'GET'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_user_add_push_tokens():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid user token or not provided at all")

	push_tokens = json.loads(user.push_tokens)
	origin, push_token = request.args.get('origin'), request.args.get('pushtoken')
	if None not in [origin, push_token]:
		p_token = {'origin': origin, 'push_token': push_token}
		push_tokens.append(p_token)
		push_tokens = [dict(t) for t in {tuple(d.items()) for d in push_tokens}] #removes duplicates
		user.push_tokens = json.dumps(push_tokens)
		db.session.commit()
	return api_response(ret=user.serialize())


@app.route('/foodfare/login_user', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_login_user():
	pwd = request.form['password']
	email = request.form['email']
	user = FoodfareUser.query.filter_by(email=email, password=pwd).first()
	if user is None:
		return api_response(error="Invalid email or password, please try again!")
	user.token = uuid.uuid4()
	db.session.commit()
	return api_response(ret=user.serialize())


@app.route('/foodfare/get_user_profile', methods=['GET', 'POST'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_get_user_profile():
	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid token or not provided at all")

	return api_response(ret=user.serialize())


@app.route('/foodfare/edit_user_profile', methods=['GET', 'POST'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_edit_user_profile():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid token or not provided at all")

	params = request.get_json()
	if params is None:
		params = request.form

	for k, v in params.iteritems():
		if v == 'true':
			v = True
		elif v == 'false':
			v = False
		setattr(user, k, v)
	db.session.commit()

	return api_response(ret=user.serialize())


@app.route('/foodfare/change_user_password', methods=['GET', 'POST'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
def foodfare_change_user_password():

	user = get_guest_from_api_call()
	if user is None:
		return api_response(error="Invalid token or not provided at all")

	params = request.get_json()
	if params is None:
		params = request.form

	for p in ['current_password', 'new_password', 'verify_password']:
		if p not in params:
			return api_response(error='For security reasons, please fill the entire form again.')

	if params['current_password'] != user.password:
		return api_response(error="Please insert correct password")
	if params['new_password'] != params['verify_password']:
		return api_response(error="Please verify new password")

	user.password = params['new_password']
	db.session.commit()

	return api_response()


@app.route('/foodfare/forgot_password', methods=['POST'])
def foodfare_forgot_password():
	email = request.form['email']
	security_answer = request.form['security_answer']

	user = FoodfareUser.query.filter_by(email=email, security_answer=security_answer).first()
	if user is None:
		return api_response(error="Invalid email or security answer, please try again!")

	subject = 'Password Reminder'
	html = ("Hi, <br><br>You requested a reminder of your Foodfare password! You can log into the app with:<br><br><strong>"+str(user.password)+
			"</strong><br><br>Thank you, <br><br>-Foodfare<br><br><img src='http://foodfare2.butlerpad.com/static/img/foodfare_logo.jpg'"
			"alt='Foodfare logo' width='250'>")

	mail_sent = mail_sendgrid(to=email, subject=subject, sender='noreply@foodfare.com.sg', html=html)
	if not mail_sent:
		return api_response(error='Email message not sent')
	return api_response()


@app.route('/foodfare/forgot_passcode', methods=['POST'])
def foodfare_forgot_passcode():
	email = request.form['email']
	security_answer = request.form['security_answer']
	passcode = request.form['passcode']

	user = FoodfareUser.query.filter_by(email=email, security_answer=security_answer).first()
	if user is None:
		return api_response(error="Invalid email or security answer, please try again!")

	subject = 'Passcode Reminder'
	html = ("Hi, <br><br>You requested a reminder of your Foodfare passcode! You can log into the app with:<br><br><strong>"+str(passcode)+
			"</strong><br><br>Thank you, <br><br>-Foodfare<br><br><img src='http://foodfare2.butlerpad.com/static/img/foodfare_logo.jpg'"
			"alt='Foodfare logo' width='250'>")

	mail_sent = mail_sendgrid(to=email, subject=subject, sender='noreply@foodfare.com.sg', html=html)
	if not mail_sent:
		return api_response(error='Email message not sent')
	return api_response()



@app.route('/foodfare/paylah_payment_status', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*', headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def foodfare_paylah_payment_status():
	order = None
	payment_token=request.args['ptoken']
	payment = FoodfarePayment.query.filter_by(payment_token=payment_token).first()
	if not payment:
		return api_response(error="Invalid payment")

	if payment.order_id is not None:
		order = FoodfareOrder.query.get(payment.order_id)

	mid = '******************'
	payment_status_url = API_PAYLAH + '/txenquiry?txrefno={txrefno}&mcode={mid}'.format(txrefno=payment.payment_token, mid=mid)

	req_payment = requests.get(payment_status_url)

	if req_payment.status_code != 200:
		data = {'paylah_status':req_payment.status_code, 'paylah_error_msg': ''}
		prompt_admins_for_paylah_error(req_payment, req_payment.status_code)
		return api_response(error=data)

	data = req_payment.json()["data"]
	txn_status = data.get("txnStatusCode", None)
	if txn_status is not None:
		if txn_status == '0002' and payment.status != 1:
			payment.status = 1 #payment complete
			db.session.commit()
			if order and order.status == 1:
				order.status = 5 #paid
				details = json.loads(order.details)
				for item in details:
					if item['status'] == 1:
						item['status'] = 5
				order.details = json.dumps(details)
				db.session.commit()

				email = order.user_obj.email
				subject = 'Order #{orderid} payment success'.format(orderid=order.id)
				html = unicode(payment_receipt(order.id))
				mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)
		elif txn_status == "0009":
			payment.status = 2 #payment failed
			db.session.commit()
			if order and order.status == 1:
				order.status = 6 #failed
				details = json.loads(order.details)
				for item in details:
					if item['status'] == 1:
						item['status'] = 6
						stall = FoodfareStall.query.get(item['stall_id'])
						timeslots = json.loads(stall.timeslots)
						for timeslot in timeslots:
							if timeslot['time'] == item['collection_time']:
								timeslot['open'] += item['quantity']
							if timeslot['open'] > timeslot['slots']:
								timeslot['open'] = timeslot['slots']
						stall.timeslots = json.dumps(timeslots)
				order.details = json.dumps(details)
				db.session.commit()
				email = order.user_obj.email
				subject = 'Order #{orderid} payment failed'.format(orderid=order.id)
				html = unicode(payment_receipt(order.id))
				mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)
		elif txn_status == "0003":
			payment.status = 3 #payment cancelled
			db.session.commit()
			if order:
				#void_paylah_txn('', payment)
				order.status = 4 #cancelled
				details = json.loads(order.details)
				for item in details:
					item['status'] = 4
					stall = FoodfareStall.query.get(item['stall_id'])
					timeslots = json.loads(stall.timeslots)
					for timeslot in timeslots:
						if timeslot['time'] == item['collection_time']:
							timeslot['open'] += item['quantity']
						if timeslot['open'] > timeslot['slots']:
							timeslot['open'] = timeslot['slots']
					stall.timeslots = json.dumps(timeslots)
				order.details = json.dumps(details)
				db.session.commit()
				email = order.user_obj.email
				subject = 'Order #{orderid} payment cancelled'.format(orderid=order.id)
				html = unicode(payment_receipt(order.id))
				mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)
		return api_response(ret=data)
	return api_response(error=data)


@celery_foodfare.task(name='app.paylah_payment_status')
def paylah_payment_status(payment_token):
	elapsed_time = 0
	while True:
		if elapsed_time <= 600:
			while True:
				try:
					req_payment = requests.post('http://foodfare2.butlerpad.com/foodfare/paylah_payment_status?ptoken={ptoken}'.format(ptoken=payment_token))
					res_payment = req_payment.json()
					if res_payment['success'] is True:
						txn_status = res_payment['ret'].get("txnStatusCode")
						if txn_status is not None:
							if txn_status == '0002': #payment complete
								print 'Payment complete, email sent'
								return
							elif txn_status == "0009": #payment failed
								print 'Payment failed, email sent'
								return
							elif txn_status == '0003': #payment complete
								print 'Payment cancelled, email sent'
								return
						else:
							print 'txn_status is missing'
							#return
					else:
						error_status = res_payment['error'].get("statusCode")
						if error_status != '9999':
							#app.logger.info(res_payment)
							#app.logger.info(error_status)
							print res_payment
							print error_status
				except Exception as e:
					html = '{exc_msg} Payment token:{payment_token}'.format(exc_msg=e.message, payment_token=payment_token)
					mail_sendgrid(to='perakism1982@yahoo.gr', subject='Celery Foodfare2 Paylah payment status {exc_type} ERROR'.format(exc_type=type(e)), sender='noreply@error.monitor.btg', bcc='paul@bieh.net', html=html)
					elapsed_time += 1
					print elapsed_time
					time.sleep(1)
					continue
				break
			elapsed_time += 10
			print elapsed_time
			time.sleep(10)
		else:
			payment = FoodfarePayment.query.filter_by(payment_token=payment_token).first()
			order = None

			if payment is not None:
				if payment.order_id:
					order = app_order.query.get(payment.order_id)
					if order and order.status == 1:
						order.status = 6
						details = json.loads(order.details)
						for item in details:
							if item['status'] == 1:
								item['status'] = 6
								stall = FoodfareStall.query.get(item['stall_id'])
								timeslots = json.loads(stall.timeslots)
								for timeslot in timeslots:
									if timeslot['time'] == item['collection_time']:
										timeslot['open'] += item['quantity']
									if timeslot['open'] > timeslot['slots']:
										timeslot['open'] = timeslot['slots']
								stall.timeslots = json.dumps(timeslots)
						order.details = json.dumps(details)
						db.session.commit()
				if payment.status == 0:
					payment.status = 2
				db.session.commit()
			return


@celery_foodfare.task(name='app.wirecard_payment_status')
def wirecard_payment_status(payment_token):
	elapsed_time = 0
	while True:
		if elapsed_time <= 600:
			while True:
				try:
					req = requests.post('http://foodfare2.butlerpad.com/foodfare/wirecard_payment_status?ptoken={ptoken}'.format(ptoken=payment_token))
					res = req.json()
					if res['success'] is True:
						payments = res['ret']['payments']
						if payments:
							if payments['payment'][0]['transaction-state'] not in ['success', 'failed']:
								#app.logger.info(payments)
								print payments
							return
				except Exception as e:
					html = '{exc_msg} Payment token:{payment_token}'.format(exc_msg=e.message, payment_token=payment_token)
					mail_sendgrid(to='perakism1982@yahoo.gr', subject='Celery Foodfare Wirecard payment status {exc_type} ERROR'.format(exc_type=type(e)), sender='noreply@error.monitor.btg', bcc='paul@bieh.net', html=html)
					elapsed_time += 1
					time.sleep(1)
					continue
				break
			elapsed_time += 10
			print elapsed_time
			time.sleep(10)
		else:
			payment = FoodfarePayment.query.filter_by(payment_token=payment_token).first()
			order = None

			if payment is not None:
				if payment.order_id:
					order = FoodfareOrder.query.get(payment.order_id)
					if order and order.status == 1:
						order.status = 6
						details = json.loads(order.details)
						for item in details:
							if item['status'] == 1:
								item['status'] = 6
								stall = FoodfareStall.query.get(item['stall_id'])
								timeslots = json.loads(stall.timeslots)
								for timeslot in timeslots:
									if timeslot['time'] == item['collection_time']:
										timeslot['open'] += item['quantity']
									if timeslot['open'] > timeslot['slots']:
										timeslot['open'] = timeslot['slots']
								stall.timeslots = json.dumps(timeslots)
						order.details = json.dumps(details)
						db.session.commit()
				if payment.status == 0:
					payment.status = 2
				db.session.commit()
			return


@app.route('/wirecard_payment/<status>', methods=['GET', 'POST', 'OPTIONS'])
@crossdomain(origin='*', methods=['GET', 'POST', "OPTIONS"], headers=['X-Requested-With', 'Content-Type', 'Origin','X-XSRF-TOKEN'])
@nocache
def set_wirecard_payment_status(status):

	#Debug logging
	#app.logger.info('REQUEST URL '+request.url)
	#app.logger.info('REQUEST RAW DATA '+unicode(request.__dict__))

	print 'REQUEST URL '+request.url
	print 'REQUEST RAW DATA '+unicode(request.__dict__)

	redirect_url = request.args.get('redirecturl')
	ptoken = request.args['ptoken']
	resp = ''

	payment = FoodfarePayment.query.filter_by(payment_token=ptoken).first()
	if payment is None:
		return api_response(error="Invalid payment token")

	order = FoodfareOrder.query.filter_by(id=payment.order_id).first()
	payment.timestamp = time.time()

	if status == "success":
		payment.status = 1
		if order and order.status == 1:
			order.status = 5
			details = json.loads(order.details)
			for item in details:
				if item['status'] == 1:
					item['status'] = 5
			order.details = json.dumps(details)
			email = order.user_obj.email
			subject = 'Order #{orderid} payment success'.format(orderid=order.id)
			html = unicode(payment_receipt(order.id))
			mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)
		db.session.commit()

		if request.form:
			transaction_id = request.form.get('transaction_id')
			payment.transaction_id = transaction_id
			db.session.commit()
			if transaction_id is None:
				#app.logger.info(request.form)
				print request.form
			if payment.save is True:
				wallet = FoodfareWallet.query.filter_by(user_id=payment.user_id, payment_token=request.form['token_id'], masked_card_num=request.form['masked_account_number']).first()
				if wallet is None:
					#adds new payment method
					wallet = FoodfareWallet(payment.user_id, 'wirecard', request.form['token_id'], request.form['masked_account_number'])
					db.session.add(wallet)
					db.session.commit()

				if payment.cancel is True:
					req = void_wirecard_txn(payment)
					if req.get_json()['success'] is False:
						return api_response(error='Payment refund failed')
		return api_response()
	elif status == "fail":
		payment.status = 2
		if order and order.status == 1:
			order.status = 6
			details = json.loads(order.details)
			for item in details:
				if item['status'] == 1:
					item['status'] = 6
					stall = FoodfareStall.query.get(item['stall_id'])
					timeslots = json.loads(stall.timeslots)
					for timeslot in timeslots:
						if timeslot['time'] == item['collection_time']:
							timeslot['open'] += item['quantity']
						if timeslot['open'] > timeslot['slots']:
							timeslot['open'] = timeslot['slots']
					stall.timeslots = json.dumps(timeslots)
			order.details = json.dumps(details)
			email = order.user_obj.email
			subject = 'Order #{orderid} payment failed'.format(orderid=order.id)
			html = unicode(payment_receipt(order.id))
			mail_sent = mail_sendgrid(to=email, subject=subject, sender="noreply@foodfare.com.sg", html=html)
		db.session.commit()
		return api_response(error="Payment failed")
	else:
		return api_response(error="Payment status undefined")


##############################
# Generic API (end)
##############################

###############################
## CMS admin
###############################

def admins():
	return FoodfareAdmin.query.all()


@login_manager.user_loader
def user_loader(email):
	for adm in admins():
		if email == adm.email:
			return adm
	return None


@login_manager.unauthorized_handler
def unauthorized_callback():
	return redirect(url_for('cms_admin_login'))


@app.route('/foodfare/admin/login', methods=['GET', 'POST', 'OPTIONS'])
def cms_admin_login():
	if request.method == "POST":

		email = request.form['email']
		password = request.form['password']
		remember_me = False

		if 'remember' in request.form:
			remember_me = True

		admin = FoodfareAdmin.query.filter_by(email=email).first()
		if admin is None:
			return abort(401)

		if password == admin.password:
			flask_login.login_user(admin, remember=remember_me)
			page = 'cms_admin_orders'
			if admin.role == 2:
				page = 'cms_admin_menus'
			return redirect(url_for(page))

	return render_template('admin_login.html')


@app.route("/foodfare/admin/logout")
@login_required
def cms_admin_logout():
	logout_user()
	return redirect(url_for('cms_admin_login'))


@app.route('/foodfare/admin/stores', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
def cms_admin_stores():
	if current_user.role != 0:
		return abort(401)
	stores = FoodfareStore.query.filter_by(visible=True).all()
	return render_template("stores.html", stores=stores)


@app.route('/foodfare/admin/store/<storeid>/stalls', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_stalls(storeid):
	if current_user.role != 0:
		return abort(401)
	store = FoodfareStore.query.get(storeid)
	stalls = FoodfareStall.query.filter_by(store_id=store.id).all()
	return render_template("stalls.html", store=store, stalls=stalls)


@app.route('/foodfare/admin/store/<storeid>/stall/<stallid>', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_stall(stallid, storeid):
	from collections import OrderedDict
	if current_user.role != 0:
		return abort(401)
	stall = FoodfareStall.query.get(stallid)
	timeslots = []
	if stall:
		timeslots = json.loads(stall.timeslots, object_pairs_hook=OrderedDict)

	#if request.method == "GET":
	if 'delete' in request.args:
		FoodfareMenuItem.query.filter_by(stall_id=stall.id).delete(synchronize_session=False)
		db.session.commit()

		db.session.delete(stall)
		db.session.commit()
		return redirect(url_for('cms_admin_stalls', storeid=storeid))

	elif 'delete_timeslot' in request.args:
		timeslots = [timeslot for timeslot in timeslots if timeslot['time'] != request.args['time']]
		stall.timeslots = json.dumps(timeslots)
		db.session.commit()
		return redirect(request.referrer)

	elif 'add_timeslot' in request.args:
		timeslots.append({"time": "00:00","slots":0,"open":0})
		stall.timeslots = json.dumps(timeslots)
		db.session.commit()
		return redirect(request.referrer)

	if request.method == "POST":
		file = request.files.get('image')
		if file and allowed_file(file.filename):
			filename = file.filename
			fullpath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
			modified_filename = filename[:filename.find('.')]+'_400x400'+filename[filename.find('.'):]
			modified_path = os.path.join(app.config['UPLOAD_FOLDER'], modified_filename)
			file.save(fullpath)
			resize_and_crop(fullpath, modified_path, [400,  400], crop_type='middle')
			stall.image = request.host_url+'static/uploads/'+modified_filename
			db.session.commit()

		if 'shown' in request.form: #if checkbox is checked
			stall.shown = True
		else:
			stall.shown = False

		if 'auto_open_close' in request.form: #if checkbox is checked
			stall.auto_open_close = True
		else:
			stall.auto_open_close = False

		for k, v in request.form.iteritems():
			if k != 'image':
				if k == 'tags':
					v = json.dumps([s.strip() for s in v.split(',')])
				if v == 'None':
					v = None

				if k.startswith('time'):
					keys = k.split('_')
					for timeslot in timeslots:
						if timeslot['time'] == keys[-1]:
							timeslot['time'] = v
					k = 'timeslots'
					v = json.dumps(timeslots)

				if k.startswith('slots'):
					keys = k.split('_')
					for timeslot in timeslots:
						if timeslot['time'] == keys[-1]:
							timeslot['slots'] = int(v)
							timeslot['open'] = int(v)
					k = 'timeslots'
					v = json.dumps(timeslots)

				setattr(stall, k, v)
		db.session.commit()
		return redirect(request.referrer)
	return render_template("stall.html", stall=stall, timeslots=timeslots)


@app.route('/foodfare/admin/store/<storeid>/stall/<stallid>/menu', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_menu(storeid, stallid):
	if stallid is None:
		return abort(404)
	stall = FoodfareStall.query.get(stallid)
	menu = FoodfareMenuItem.query.filter_by(stall_id=stallid).all()
	return render_template("menu.html", menu=menu, stall=stall)


@app.route('/foodfare/admin/store/<storeid>/stall/<stallid>/menu/<itemid>', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_menu_item(storeid, stallid, itemid):
	if current_user.role != 0:
		return abort(401)

	if 'new' == itemid and request.method == "GET":
		menu_item = FoodfareMenuItem()
		menu_item.stall_id = request.args['stallid']
		db.session.add(menu_item)
		db.session.commit()
		return redirect(url_for('cms_admin_menu_item',storeid=storeid, stallid=stallid, itemid=menu_item.id))

	item = FoodfareMenuItem.query.get(itemid)
	stall = FoodfareStall.query.get(item.stall_obj.id)

	if 'delete' in request.args:
		db.session.delete(item)
		db.session.commit()
		return redirect(url_for('cms_admin_menu',storeid=storeid, stallid=stall.id))
	elif 'delete_sel' in request.args:
		name = request.args['name']
		name = None if name =='None' else name
		selections = item.selections
		selections = [i for i in json.loads(selections) if i['name'] != name]
		item.selections = json.dumps(selections)
		db.session.commit()
		return redirect(request.referrer)
	elif 'delete_sub' in request.args:
		name = request.args['name']
		name = None if name =='None' else name
		submenu = item.submenu
		submenu = [i for i in json.loads(submenu) if i['name'] != name]
		item.submenu = json.dumps(submenu)
		db.session.commit()
		return redirect(request.referrer)
	elif 'delete_addon' in request.args:
		name = request.args['name']
		name = None if name =='None' else name
		addons = item.addons
		addons = [i for i in json.loads(addons) if i['name'] != name]
		item.addons = json.dumps(addons)
		db.session.commit()
		return redirect(request.referrer)
	elif 'add_sel' in request.args:
		selections = item.selections[:-1]+'{"name":null,"price":null,"code":null}'+']'
		selections = selections.replace("}{", "},{")
		item.selections = selections
		db.session.commit()
		return redirect(request.referrer)
	elif 'add_sub' in request.args:
		submenu = item.submenu[:-1]+'{"name":null,"price":null,"code":null}'+']'
		submenu = submenu.replace("}{", "},{")
		item.submenu = submenu
		db.session.commit()
		return redirect(request.referrer)
	elif 'add_addon' in request.args:
		addons = item.addons[:-1]+'{"name":null,"price":null,"code":null}'+']'
		addons = addons.replace("}{", "},{")
		item.addons = addons
		db.session.commit()
		return redirect(request.referrer)

	elif request.method == "POST":
		file = request.files.get('image')
		if file and allowed_file(file.filename):
			filename = file.filename
			fullpath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
			modified_filename = filename[:filename.find('.')]+'_400x400'+filename[filename.find('.'):]
			modified_path = os.path.join(app.config['UPLOAD_FOLDER'], modified_filename)
			file.save(fullpath)
			resize_and_crop(fullpath, modified_path, [400,  400], crop_type='middle')
			item.image = request.host_url+'static/uploads/'+modified_filename
			db.session.commit()

		if 'shown' in request.form: #if checkbox is checked
			item.shown = True
		else:
			item.shown = False

		if 'special_offers' in request.form: #if checkbox is checked
			item.special_offers = True
		else:
			item.special_offers = False

		for k, v in request.form.iteritems():
			if k != 'image':
				if k == 'tags':
					v = json.dumps([s.strip() for s in v.split(',')])
				if v == 'None':
					v = None

				for i in ['selections', 'submenu', 'addons']:
					if k.startswith(i):
						keys = k.split('_')
						data = json.loads(getattr(item, i))
						index = int(keys[-1])-1
						data[index][keys[1]] = v
						v = json.dumps(data)
						k = i
				setattr(item, k, v)

		db.session.commit()
		return redirect(url_for('cms_admin_menu',storeid=storeid, stallid=item.stall_obj.id))
	return render_template("menu_item.html", item=item, stall=stall,
			selections=json.loads(item.selections), submenu=json.loads(item.submenu),
			addons=json.loads(item.addons))


@app.route('/foodfare/admin/orders', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_orders():
	stores = FoodfareStore.query.all()
	return render_template("orders.html", stores=stores)


@app.route('/foodfare/admin/feedback', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_feedback():
	return render_template("feedback.html")


@app.route('/foodfare/admin/users', methods=['GET', 'POST', 'OPTIONS'])
@login_required
@nocache
def cms_admin_users():
	if current_user.role != 0:
		return abort(401)
	return render_template("users.html")


@app.route('/foodfare/admin/user/<uid>', methods=['GET', 'POST', 'OPTIONS'])
@login_required
@nocache
def cms_admin_get_user(uid):
	user = FoodfareUser.query.get(uid)
	orders_w_promos = FoodfareOrder.query.filter(FoodfareOrder.user_id==user.id, FoodfareOrder.promo_id != None, FoodfareOrder.status.in_([2,3,5])).all()
	stores = FoodfareStore.query.all()
	orders_history = FoodfareOrder.query.filter(FoodfareOrder.user_id==uid, FoodfareOrder.status!=1).order_by(FoodfareOrder.timestamp.desc()).all()

	store_id = request.args.get('storeid')
	from_date = string2datetime(request.args.get('fromdate', ''))
	to_date = string2datetime(request.args.get('todate', ''), _from=False)

	if 'reset' in request.args:
		promo_id = request.args['promoid']
		order_promo = FoodfareOrder.query.filter_by(promo_id=promo_id, user_id=user.id).first()
		order_promo.promo_id = None
		db.session.commit()
		return redirect(request.referrer)

	orders = []
	for order in orders_history:
		order_date = datetime.datetime.fromtimestamp(order.timestamp)
		if from_date <= order_date <= to_date + datetime.timedelta(days=1):
			if store_id not in [None, 'all']:
				if order.store_id == int(store_id):
					orders.append(order)
			else:
				orders.append(order)

	return render_template("user_profile.html", user=user, orders=orders, promos=orders_w_promos, stores=stores,
		from_date=from_date.strftime('%Y-%m-%d'), to_date=to_date.strftime('%Y-%m-%d'))


@app.route('/foodfare/admin/payment_transactions', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_payment_transactions():
	all_ = request.args.get('all')
	stores = FoodfareStore.query.all()
	return render_template("transactions.html", stores=stores, all_=all_)


@app.route('/foodfare/admin/promos', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_promos():
	promoconfigs = FoodfarePromoConfig.query.all()
	promos = []
	for promo_config in promoconfigs:
		if promo_config.code is None: # delete empty promos
			db.session.delete(promo_config)
			db.session.commit()

		promos_used_num = FoodfareOrder.query.filter_by(promo_id=promo_config.id).count()
		promo_config = promo_config.serialize()
		promo_config['used'] = promos_used_num
		promos.append(promo_config)

	return render_template("promos.html", promos=promos)

@app.route('/foodfare/admin/ads', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_ads():
	if current_user.role != 0:
		return abort(401)
	ads = FoodfareAd.query.all()
	return render_template("ads.html", ads=ads)


@app.route('/foodfare/admin/promo/<promoid>', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_promo_edit(promoid):
	if current_user.role != 0:
		return abort(401)

	if 'new' in promoid and request.method == "GET":
		promo_config = FoodfarePromoConfig()
		db.session.add(promo_config)
		db.session.commit()
		return redirect("/foodfare/admin/promo/{id}".format(id=str(promo_config.id)))

	promo_config = FoodfarePromoConfig.query.get(promoid)

	if 'delete' in request.args:
		orders = FoodfareOrder.query.filter_by(promo_id=promo_config.id).all()
		for order in orders:
			order.promo_id = None
			remarks = json.loads(order.remarks)
			remarks.update({'promo_id':promo_config.id,'status': 'deleted'})
			order.remarks = json.dumps(remarks)
			db.session.commit()
		db.session.delete(promo_config)
		db.session.commit()

		return redirect(url_for('cms_admin_promos'))

	if request.method == "POST":

		if 'active' in request.form: #if checkbox is checked
			promo_config.active = True
		else:
			promo_config.active = False

		for k, v in request.form.iteritems():
			if v == 'None':
				v = None
			setattr(promo_config, k, v)
		db.session.commit()
		return redirect(url_for('cms_admin_promos'))

	return render_template("promoconfig_details.html", promo_config=promo_config)


from PIL import Image

def resize_and_crop(img_path, modified_path, size, crop_type='middle'):

	# If height is higher we resize vertically, if not we resize horizontally
	img = Image.open(img_path)
	# Get current and desired ratio for the images
	img_ratio = img.size[0] / float(img.size[1])
	ratio = size[0] / float(size[1])
	#The image is scaled/cropped vertically or horizontally depending on the ratio
	if ratio > img_ratio:
		img = img.resize((size[0], int(round(size[0] * img.size[1] / img.size[0]))),
			Image.ANTIALIAS)
		# Crop in the top, middle or bottom
		if crop_type == 'top':
			box = (0, 0, img.size[0], size[1])
		elif crop_type == 'middle':
			box = (0, int(round((img.size[1] - size[1]) / 2)), img.size[0],
				int(round((img.size[1] + size[1]) / 2)))
		elif crop_type == 'bottom':
			box = (0, img.size[1] - size[1], img.size[0], img.size[1])
		else :
			raise ValueError('ERROR: invalid value for crop_type')
		img = img.crop(box)
	elif ratio < img_ratio:
		img = img.resize((int(round(size[1] * img.size[0] / img.size[1])), size[1]),
			Image.ANTIALIAS)
		# Crop in the top, middle or bottom
		if crop_type == 'top':
			box = (0, 0, size[0], img.size[1])
		elif crop_type == 'middle':
			box = (int(round((img.size[0] - size[0]) / 2)), 0,
				int(round((img.size[0] + size[0]) / 2)), img.size[1])
		elif crop_type == 'bottom':
			box = (img.size[0] - size[0], 0, img.size[0], img.size[1])
		else :
			raise ValueError('ERROR: invalid value for crop_type')
		img = img.crop(box)
	else :
		img = img.resize((size[0], size[1]),
			Image.ANTIALIAS)
	# If the scale is the same, we do not need to crop
	img.save(modified_path)


@app.route('/foodfare/admin/ad/<adid>', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_ad_edit(adid):

	if 'new' in adid and request.method == "GET":
		ad = FoodfareAd()
		db.session.add(ad)
		db.session.commit()
		return redirect("/foodfare/admin/ad/{id}".format(id=str(ad.id)))

	ad = FoodfareAd.query.get(adid)

	if 'delete' in request.args:
		db.session.delete(ad)
		db.session.commit()
		return redirect(url_for('cms_admin_ads'))

	if request.method == "POST":
		file = request.files.get('image')
		if file and allowed_file(file.filename):
			filename = file.filename
			fullpath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
			modified_filename = filename[:filename.find('.')]+'_375x208'+filename[filename.find('.'):]
			modified_path = os.path.join(app.config['UPLOAD_FOLDER'], modified_filename)
			file.save(fullpath)
			resize_and_crop(fullpath, modified_path, [375,  208], crop_type='middle')
			ad.image = request.host_url+'static/uploads/'+modified_filename
			db.session.commit()

		if 'active' in request.form: #if checkbox is checked
			ad.active = True
		else:
			ad.active = False

		for k, v in request.form.iteritems():
			if k != 'image':
				if v == 'None':
					v = None
				setattr(ad, k, v)
		db.session.commit()
		return redirect(request.referrer)

	return render_template("ad_details.html", ad=ad)


def string2datetime(s, _from=True):
	if s == "" and _from:
		s = '2016-01-01'
	elif s == "":
		return datetime.datetime.now()
	return datetime.datetime.strptime(s, '%Y-%m-%d')


@app.route('/foodfare/admin/send_notification', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
@nocache
def cms_admin_send_notification():
	params = request.form
	user_id = params['user_id']
	subject = params['subject']
	body = params['body']
	s = send_push_notification(user_id, subject, body)
	#app.logger.info("#####################################"+s)
	print "#####################################"+s
	flash('Notification sent')
	return redirect(request.referrer)

##############################
# CMS Admin (end)
##############################



##############################
# EDSR -- stall summary
##############################
@app.route('/foodfare/edsr/generate/stall_summary/<stallid>')
def get_stall_summary_edsr(stallid):

	if stallid == "all":
		for s in FoodfareStall.query.all():

			edsr = generate_stall_summary_edsr(s.id, testmode=request.args.get("test") == "true")

			if not edsr:
				continue

			# write it out to tmp
			of = open("/tmp/edsr/" + edsr["filename"], "w")
			of.write(edsr['text'])
			of.close()

		return "ok"
	else:

		edsr = generate_stall_summary_edsr(stallid, testmode=True)

		if not edsr:
			return "error"

		of = open("/tmp/edsr/" + edsr["filename"], "w")
		of.write(edsr['text'])
		of.close()

		return send_file("/tmp/edsr/" + edsr["filename"], as_attachment=True)


def generate_stall_summary_edsr(stallid, testmode=False):
	ret = ""

	stall = FoodfareStall.query.get(stallid)

	if not stall or not stall.terminal_id:
		return None

	total_qty = 0
	total_amount = 0
	total_reciept = 0
	total_cash = 0
	total_credit_card = 0
	total_voucher = 0
	total_link = 0

	for i in FoodfareOrder.query.filter_by(in_pos=0).all():

		total_qty += 1

		op = FoodfarePayment.query.filter_by(order_id=i.id).first()

		if op:

			# TODO: should these be different?
			total_reciept += op.amount
			total_amount += op.amount

			# TODO: anything except 'credit card'?
			if op.payment_type == "wirecard":
				total_credit_card += op.amount

			elif op.payment_type == "paylah":
				total_credit_card += op.amount

		if not testmode:
			i.in_pos = 1

	db.session.commit()

	ret = ""

	ret += stall.terminal_id + "|"
	ret += datetime.date.today().strftime("%Y-%m-%d") + "|"
	ret += str(total_qty) + "|"
	ret += str(total_amount) + "|"
	ret += str(total_reciept) + "|"
	ret += str(total_cash) + "|"
	ret += str(total_credit_card) + "|"
	ret += str(total_voucher) + "|"
	ret += str(total_link) + "|"

	for i in range(0, 6):
		ret += "|''|0.0"

	return {"text": ret, "filename": stall.terminal_id + "_" + datetime.date.today().strftime("%Y%m%d") + ".dat"}







##############################
# EDSR -- order summary
##############################
@app.route('/foodfare/edsr/generate/order/<orderid>')
def get_order_edsr(orderid):

	if orderid == "all":
		for i in FoodfareOrder.query.filter_by(in_pos=0).all():

			edsr = generate_order_edsr(i.id, testmode=request.args.get("test") == "true")

			if not edsr:
				continue

			# write it out to tmp
			of = open("/tmp/edsr/" + edsr["filename"], "w")
			of.write(edsr['text'])
			of.close()

		return "ok"
	else:

		edsr = generate_order_edsr(orderid, testmode=True)

		if not edsr:
			return "error"

		of = open("/tmp/edsr/" + edsr["filename"], "w")
		of.write(edsr['text'])
		of.close()

		return send_file("/tmp/edsr/" + edsr["filename"], as_attachment=True)



def generate_order_edsr(orderid, testmode=False):

	order = FoodfareOrder.query.get(orderid)

	if not order:
		print "Bad order"
		return None

	op = FoodfarePayment.query.filter_by(order_id=order.id).first()

	if not op:
		print "Bad payment"
		return None

	try:
		alldata = json.loads(order.details)
	except Exception, e:
		print "Bad parsing"
		print str(e)
		return None


	ret = ""
	fn = ""

	for odata in alldata:

		stall = FoodfareStall.query.get(odata["stall_id"])

		if not stall:
			print "Bad stall", odata["stall_id"]
			return None


		ret += str(stall.id) + "|"
		ret += datetime.datetime.utcfromtimestamp(order.timestamp).strftime('%Y-%m-%d %H:%M:%S') + "|"
		ret += str(order.id) + "|"
		ret += str(odata["id"] if "id" in odata else 0) + "|"
		ret += str(odata["menu_item"]) + "|"
		ret += str(odata["quantity"]) + "|"
		ret += str(odata["price"]) + "|"
		ret += str(0.0) + "|"
		ret += str(odata["price"]) + "|"
		ret += str(op.payment_type) + "|"
		ret += str(op.amount)
		ret += "\n"

		fn = str(stall.id) + "_" + datetime.datetime.utcfromtimestamp(order.timestamp).strftime('%Y%m%d_%H%M%S') + ".dat"

	return {"text": ret.strip(), "filename": fn}


################################################################################
# Reporting
################################################################################
def report_daily_stall_sale(date_start, date_end):
	ret = []

	perdate = {}

	for i in daterange(date_start, date_end):
		sales = sales_per_stall_for_date(i)

		for stall in FoodfareStall.query.all():

			if stall.id not in sales:
				#sales[stall.id] = []
				continue

			row = OrderedDict()

			row["Stall ID"] = stall.id
			row["Stall Name"] = stall.name
			row["Date"] = i.strftime("%Y-%m-%d")
			row.update(standard_sales_calcs(sales[stall.id]))

			ret.append(row)

	return ret

def report_daily_stall_sale_by_tag(date_start, date_end):
	ret = []

	perdate = {}

	for i in daterange(date_start, date_end):
		all_sales = sales_for_date(i)

		sales_by_tag_by_stall = {}
		for s in all_sales:
			item = FoodfareMenuItem.query.get(s["id"])
			stallid = s["stall_id"]

			if stallid not in sales_by_tag_by_stall:
				sales_by_tag_by_stall[stallid] = {}

			for t in json.loads(item.tags):
				if len(t.strip()) == 0:
					continue
				if t not in sales_by_tag_by_stall[stallid]:
					sales_by_tag_by_stall[stallid][t] = []
				sales_by_tag_by_stall[stallid][t].append(s)

		for stallid,tags in sales_by_tag_by_stall.iteritems():
			for tag, sales in tags.iteritems():

				row = OrderedDict()

				stall = FoodfareStall.query.get(stallid)

				row["Stall ID"] = stall.id
				row["Stall Name"] = stall.name
				row["Tag"] = tag
				row["Date"] = i.strftime("%Y-%m-%d")

				row.update(standard_sales_calcs(sales))

				ret.append(row)

	return ret

def report_hourly(date_start, date_end):
	ret = []

	perhour = {}
	sales = []

	for i in daterange(date_start, date_end):
		sales.extend(sales_for_date(i))

	for s in sales:
		hour = datetime.datetime.utcfromtimestamp(s["order_object"].timestamp).strftime('%H:00')

		if hour not in perhour:
			perhour[hour] = []

		perhour[hour].append(s)

	for k,v in perhour.iteritems():
		row = OrderedDict()
		row["Hour"] = k
		row["Quantity"] = len(v)
		row["Amount"] = sum(float(item['price']) for item in v) + sum(float(item['take_away']) for item in v)

		ret.append(row)

	return ret

def report_outlet_item(date_start, date_end):
	ret = []

	peritem = {}
	all_sales = []

	for store in FoodfareStore.query.all():

		for i in daterange(date_start, date_end):
			daysales = sales_for_date(i, store_id=store.id)
			all_sales.extend(daysales)

		for s in all_sales:
			if s["menu_item"] not in peritem:
				peritem[s["menu_item"]] = []
			peritem[s['menu_item']].append(s)

		for menu_item, sales in peritem.iteritems():

			row = OrderedDict()

			row["Date"] = i.strftime("%Y-%m-%d")
			row["Outlet"] = store.name
			row["Item"] = menu_item
			row.update(standard_sales_calcs(sales))

			ret.append(row)

	return ret



def report_stall_tender(date_start, date_end):
	ret = []


	for stall in FoodfareStall.query.all():
		per_method = {}

		for i in daterange(date_start, date_end):
			for p in payments_filter_by_stallid(payments_for_date(i), stall.id):
				m = p.payment_type + " - " + p.mode

				if m not in per_method:
					per_method[m] = []
				per_method[m].append(p)

		for k, v in per_method.iteritems():
			row = OrderedDict()
			row["Tender"] = k
			row["Stall ID"] = stall.id
			row["Stall"] = stall.name
			row["Quantity"] = len(v)
			row["Amount"] = sum(float(item.amount) for item in v)
			row["Unused Amount"] = 0.0

			ret.append(row)

	return ret

def payments_filter_by_stallid(payments, stallid):
	ret = []
	for p in payments:
		if not p.order_id:
			continue
		o = FoodfareOrder.query.get(p.order_id)

		if not o:
			continue

		d = o.serialize()["details"]
		if d[0]["stall_id"] == stallid:
			ret.append(p)
	return ret



def report_tender(date_start, date_end):
	ret = []

	per_method = {}

	for i in daterange(date_start, date_end):
		for p in payments_for_date(i):
			m = p.payment_type + " - " + p.mode

			if m not in per_method:
				per_method[m] = []
			per_method[m].append(p)

	for k, v in per_method.iteritems():
		row = OrderedDict()
		row["Tender"] = k
		row["Quantity"] = len(v)
		row["Amount"] = sum(float(item.amount) for item in v)
		row["Unused Amount"] = 0.0

		ret.append(row)

	return ret


def report_promos(date_start, date_end):
	ret = []
	sales = []

	for i in daterange(date_start, date_end):
		sales.extend(sales_for_date(i))

	for s in sales:
		if s["promo_amount"] == 0:
			continue

		stall = FoodfareStall.query.get(s["stall_id"])

		row = OrderedDict()
		row["Stall ID"] = stall.id
		row["Stall"] = stall.name
		row["Timestamp"] =datetime.datetime.utcfromtimestamp(s["order_object"].timestamp).strftime('%H:%M:%S %d/%m/%Y')
		row["Item"] = s["menu_item"]
		row["Price"] = s["price"] + s["take_away"]
		row["Discount"] = s["promo_amount"]
		row["Code"] = s["promo_code"]
		row["Order ID"] = s["order_object"].id

		ret.append(row)

	return ret


def report_stall_hourly(date_start, date_end):
	ret = []

	perhour = {}
	sales = []

	for i in daterange(date_start, date_end):
		sales.extend(sales_for_date(i))

	for stall in FoodfareStall.query.all():
		stallid = stall.id

		perhour[stallid] = {}

		for s in sales:

			if s["stall_id"] != stallid:
				continue

			hour = datetime.datetime.utcfromtimestamp(s["order_object"].timestamp).strftime('%H:00')

			if hour not in perhour[stallid]:
				perhour[stallid][hour] = []

			perhour[stallid][hour].append(s)

		for k,v in perhour[stallid].iteritems():
			row = OrderedDict()
			row["Hour"] = k
			row["Stall ID"] = stallid
			row["Stall"] = stall.name
			row["Quantity"] = len(v)
			row["Amount"] = sum(float(item['price']) for item in v) + sum(float(item['take_away']) for item in v)

			ret.append(row)

	return ret

def report_stall_item(date_start, date_end):
	ret = []

	peritem = {}
	all_sales = []

	for i in daterange(date_start, date_end):
		daysales = sales_for_date(i)
		all_sales.extend(daysales)

	for s in all_sales:
		sid = s["stall_id"]
		mi = s["menu_item"]

		if sid not in peritem:
			peritem[sid] = {}

		if mi not in peritem[sid]:
			peritem[sid][mi] = []
		peritem[sid][mi].append(s)

	for stallid, stallsales in peritem.iteritems():
		for menu_item, sales in stallsales.iteritems():

			row = OrderedDict()

			row["Date"] = i.strftime("%Y-%m-%d")
			row["Stall ID"] = stallid
			row["Stall Name"] = FoodfareStall.query.get(stallid).name
			row["Item"] = menu_item
			row.update(standard_sales_calcs(sales))

			ret.append(row)

	return ret

def report_daily_outlet_sale(date_start, date_end):
	ret = []

	perdate = {}

	for store in FoodfareStore.query.all():

		for i in daterange(date_start, date_end):
			sales = sales_for_date(i, store_id=store.id)

			if len(sales) == 0:
				continue
			row = OrderedDict()

			row["Outlet"] = store.name
			row["Date"] = i.strftime("%Y-%m-%d")
			row.update(standard_sales_calcs(sales))

			ret.append(row)

	return ret




def standard_sales_calcs(sales):
	row = OrderedDict()

	row["Gross Sales"] = sum(float(item['price']) for item in sales) + sum(float(item['take_away']) for item in sales)
	row["Refund Amount"] = 0 #TODO
	row["Discount Amount"] = sum(float(item['promo_amount']) for item in sales) #TODO
	row["Total Collection"] = row["Gross Sales"] - row["Discount Amount"]
	row["GST"] = row["Gross Sales"] * 0.07
	row["Total Nett Amount"] = row["Gross Sales"] - row["GST"]
	row["QTY"] = len(sales)
	row["Total Reciept"] = len(sales)
	row["Avg Reciept"] = (row["Gross Sales"] / len(sales)) if len(sales) > 0 else 0
	row["Reciepts Printed"] = len(sales)

	return row



@app.route('/foodfare/admin/reports', methods=['GET', 'POST', 'OPTIONS'])
@flask_login.login_required
def cms_admin_reports():
	if current_user.role != 0:
		return abort(401)

	reports = {"daily_stall_sale": report_daily_stall_sale,
				"daily_stall_sale_by_tag": report_daily_stall_sale_by_tag,
				"outlet_hourly": report_hourly,
				"outlet_item": report_outlet_item,
				"tender": report_tender,
				"promos": report_promos,
				"stall_hourly": report_stall_hourly,
				"stall_item": report_stall_item,
				"stall_tender": report_stall_tender,
				"daily_outlet_sale": report_daily_outlet_sale
				}

	report = None
	data = None

	if "from" in request.args:
		date_from = datetime.datetime.strptime(request.args.get("from"), "%Y-%m-%d")
		date_to = datetime.datetime.strptime(request.args.get("to"), "%Y-%m-%d")
	else:
		date_from = datetime.date.today() - datetime.timedelta(days=1)
		date_to = datetime.date.today() + datetime.timedelta(days=0)

	if "report" in request.args:
		report = request.args.get("report")
		data = reports[report](date_from, date_to)

	return render_template("report.html",
		reports=reports.keys(), report=report, data=data,
		date_from=date_from, date_to=date_to)


# report utils
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + timedelta(n)

def sales_per_stall_for_date(date):
	ret = {}

	min_ts = time.mktime(date.timetuple())
	max_ts = time.mktime((date + datetime.timedelta(days=1)).timetuple())

	orders = FoodfareOrder.query.filter(FoodfareOrder.timestamp >= min_ts).filter(FoodfareOrder.timestamp < max_ts)

	for o in orders:
		order = o.serialize()
		details = order['details']
		for d in details:
			sid = d["stall_id"]

			if sid not in ret:
				ret[sid] = []

			d["order_object"] = o

			if o.promo_id:
				pc = FoodfarePromoConfig.query.get(o.promo_id)
				d["promo_amount"] = pc.value  / len(details)
				d["promo_code"] = pc.code
				d["promo_id"] = pc.id
			else:
				d["promo_amount"] = 0

			ret[sid].append(d)

	return ret


def sales_for_date(date, store_id=None):
	ret = {}

	min_ts = time.mktime(date.timetuple())
	max_ts = time.mktime((date + datetime.timedelta(days=1)).timetuple())

	orders = FoodfareOrder.query.filter(FoodfareOrder.timestamp >= min_ts).filter(FoodfareOrder.timestamp < max_ts)

	if store_id:
		orders = orders.filter_by(store_id=store_id)

	ret = []
	for o in orders:
		details = o.serialize()["details"]
		for d in details:
			d["order_object"] = o

			if o.promo_id:
				pc = FoodfarePromoConfig.query.get(o.promo_id)
				d["promo_amount"] = pc.value  / len(details)
				d["promo_code"] = pc.code
				d["promo_id"] = pc.id
			else:
				d["promo_amount"] = 0

		ret.extend(details)
	return ret



def payments_for_date(date):
	ret = {}

	min_ts = time.mktime(date.timetuple())
	max_ts = time.mktime((date + datetime.timedelta(days=1)).timetuple())

	payments = FoodfarePayment.query.filter(FoodfarePayment.timestamp >= min_ts).filter(FoodfarePayment.timestamp < max_ts)

	return payments.all()


################################################################################
# Send a push notifications
################################################################################
#from apns import APNs, Frame, Payload
#from pyfcm import FCMNotification
from pushjack import APNSClient, GCMClient

FCM_API_KEY = '********************'
IOS_PUSH_NOTIF_CERT = "path_to_certification"

ANDROID_CLIENT = GCMClient(api_key=FCM_API_KEY)
IOS_CLIENT = APNSClient(certificate=IOS_PUSH_NOTIF_CERT, default_retries=5)


def send_push_notification(user_id, subject, message):

	image = "path_to_logo"
	user = FoodfareUser.query.get(user_id)
	success = ''

	if user.push_tokens:
		tokens = json.loads(user.push_tokens)
		android_tokens = []
		ios_tokens = []
		for t in tokens:
			if t['origin'] == 'android' and t['push_token']:
				android_tokens.append(t['push_token'])
			elif t['origin'] == 'ios' and t['push_token']:
				ios_tokens.append(t['push_token'])

		if android_tokens:
			notification = {'title': subject, 'body': message, 'icon': image}
			alert = {'message': 'Foodfare', 'notification': notification}
			try:
				res = ANDROID_CLIENT.send(android_tokens, alert)
				success += ' Android successful push notifications: '+str(res.successes)
				for p_token in res.failures:
					tokens = [token for token in tokens if token['push_token'] != p_token]
			except Exception as e:
				success += ' Android failed push notifications: '+str(e)

		if ios_tokens:
			alert = message
			try:
				res = IOS_CLIENT.send(ios_tokens, alert, title=subject, launch_image=image)
				success += ' IOS successful push notifications: '+str(res.successes)
				for p_token in res.failures:
					tokens = [token for token in tokens if token['push_token'] != p_token]
			except Exception as e:
				success += ' IOS failed push notifications: '+str(e)

		tokens = json.dumps(tokens)
		user.push_tokens = tokens
		db.session.commit()

	return success


if __name__ == '__main__':
	app.run(debug=True)

