# -*- coding: utf-8 -*-

from foodfare2 import db
import uuid
import time
import json
import datetime

class FoodfareStore(db.Model):
	__tablename__ = 'foodfarestores'
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(1024))
	address = db.Column(db.String(1024))
	status = db.Column(db.Integer, default=1)
	image = db.Column(db.String(1024))
	opens_at = db.Column(db.String(64))
	closes_at = db.Column(db.String(64))
	disabled = db.Column(db.Boolean, default=False)
	is_uat = db.Column(db.Boolean, default=False)
	deployed = db.Column(db.Boolean, default=False)
	visible = db.Column(db.Boolean, default=True)
	geoloc_lat = db.Column(db.Float(precision=6))
	geoloc_long = db.Column(db.Float(precision=6))

	def serialize(self):
	   return {c.name: getattr(self, c.name) for c in self.__table__.columns if c.name not in ['geoloc_lat', 'geoloc_long']}


class FoodfareAdmin(db.Model):
    __tablename__ = "foodfareadmins"
    id = db.Column(db.Integer , primary_key=True)
    email = db.Column(db.String(40))
    password = db.Column(db.String(40))
    role = db.Column(db.Integer, default=0)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.email


class FoodfareStall(db.Model):
	__tablename__ = "foodfarestalls"
	id = db.Column(db.Integer, primary_key=True)
	image = db.Column(db.String(512))
	name = db.Column(db.String(512))
	shown = db.Column(db.Boolean, default=True)
	store_id = db.Column(db.Integer, db.ForeignKey('foodfarestores.id'))
	terms = db.Column(db.String(2048))
	preparation_time = db.Column(db.String(256))
	tags = db.Column(db.String(4096), default='[]') #list ex. ['Chinese', 'Vegetarian', 'Vegan', 'Indian', 'Halal']
	pin = db.Column(db.String(64))
	terminal_id = db.Column(db.String(64))
	merchant_id = db.Column(db.String(64))
	operator_id = db.Column(db.String(64))
	corporate_id = db.Column(db.String(64))
	establishment_id = db.Column(db.String(64))
	opens_at = db.Column(db.String(64))
	closes_at = db.Column(db.String(64))
	auto_open_close = db.Column(db.Boolean, default=False)
	timeslots = db.Column(db.String(4096), default='{}') #dict ex. {'ASAP': 10, '10:00': 10, '10:15': 10}
	popularity = db.Column(db.Integer, default=0)


	store_obj = db.relationship('FoodfareStore', foreign_keys='FoodfareStall.store_id')

	def serialize(self):
		data = {}
		for c in self.__table__.columns:
			value = getattr(self, c.name)
			if c.name in ['tags', 'timeslots']:
				value = json.loads(value)
			data[c.name] = value
		return data


class FoodfareMessage(db.Model):
	__tablename__ = 'foodfaremessages'
	id = db.Column(db.Integer, primary_key=True)
	subject = db.Column(db.String(512))
	body = db.Column(db.String(1024))
	timestamp = db.Column(db.Integer)  #timestamp
	unread = db.Column(db.Boolean, default=True)
	user_id = db.Column(db.Integer)

	# init
	def __init__(self, user_id, subject):
		self.subject = subject
		self.timestamp = time.time()
		self.user_id = user_id

	def serialize(self):
	   return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class FoodfareMenuItem(db.Model):
	__tablename__ = "foodfaremenuitems"
	id = db.Column(db.Integer, primary_key=True)
	details = db.Column(db.String(2048))
	image = db.Column(db.String(512))
	name = db.Column(db.String(512))
	itemno = db.Column(db.String(256))
	menu_title = db.Column(db.String(512))
	submenu = db.Column(db.String(4096), default='[]') #list of dicts ex. [{'type': 'Rice', 'price': 1.5, 'code': None}, {'type': 'Pasta', 'price': 1.0, 'code': None}]
	addons = db.Column(db.String(4096), default='[]') #list of dicts ex. [{'type': 'big', 'price': 1.5, 'code': None}, {'type': 'medium', 'price': 1.0, 'code': None}], [{'type': 'No ice', 'price': None, 'code': None}, {'type': 'Less ice', 'price': None, 'code': None}]
	selections = db.Column(db.String(4096), default='[]') #list of dicts ex. [{'type': 'big', 'price': 1.5, 'code': None}, {'type': 'medium', 'price': 1.0, 'code': None}], [{'type': 'No ice', 'price': None, 'code': None}, {'type': 'Less ice', 'price': None, 'code': None}]
	price = db.Column(db.Float)
	take_away = db.Column(db.Float)
	shown = db.Column(db.Boolean, default=True)
	special_offers = db.Column(db.Boolean, default=False)
	stall_id = db.Column(db.Integer, db.ForeignKey('foodfarestalls.id'))
	terms = db.Column(db.String(2048))
	preparation_time = db.Column(db.String(256))
	tags = db.Column(db.String(4096), default='[]') #list ex. ['Chinese', 'Vegetarian', 'Vegan', 'Healthier choice']
	popularity = db.Column(db.Integer, default=0)

	stall_obj = db.relationship('FoodfareStall', foreign_keys='FoodfareMenuItem.stall_id')

	def serialize(self):
		data = {}
		for c in self.__table__.columns:
			value = getattr(self, c.name)
			if c.name in ['tags', 'submenu', 'addons', 'selections']:
				value = json.loads(value)
			data[c.name] = value
		data['stall_name'] = self.stall_obj.name
		return data


class FoodfarePromoConfig(db.Model):
	__tablename__ = 'foodfarepromoconfig'
	id = db.Column(db.Integer, primary_key=True)
	code = db.Column(db.String(1024))
	value = db.Column(db.Float)
	issued = db.Column(db.Integer)
	active = db.Column(db.Boolean, default=True)
	timestamp = db.Column(db.Integer)
	ends_at = db.Column(db.String(128))

	# init
	def __init__(self):
		self.timestamp = time.time()

	def serialize(self):
	   return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class FoodfareUser(db.Model):
	__tablename__ = 'foodfareusers'
	id = db.Column(db.Integer, primary_key=True)
	nric = db.Column(db.String(512))
	token = db.Column(db.String(512))
	email = db.Column(db.String(512))
	password = db.Column(db.String(512))
	first_name = db.Column(db.String(512))
	last_name = db.Column(db.String(512))
	dob = db.Column(db.String(512))
	promo_emails = db.Column(db.Boolean, default=True)
	phone = db.Column(db.String(64))
	security_question = db.Column(db.String(4096))
	security_answer = db.Column(db.String(4096))
	favorites = db.Column(db.String(512), default='[]') #list ex. ['Chinese', 'Vegetarian', 'Vegan', 'Indian', 'Halal']
	push_tokens = db.Column(db.String(4096), default='[]') #list of dicts ex. [{'origin': 'ios', 'push_token': 'sdfsdf56756756dfgfgh'}, {'origin': 'ios', 'push_token': 'sdfsdf56756756dfgfgh'}]

	# init
	def __init__(self):
		self.token = uuid.uuid4()

	def serialize(self):
	   data = {}
	   for c in self.__table__.columns:
		   if c.name == 'security_answer':
			   continue
		   value = getattr(self, c.name)
		   if c.name in ['push_tokens', 'favorites']:
			   value = json.loads(value)
		   data[c.name] = value
	   return data


class FoodfareOrder(db.Model):
	__tablename__ = 'foodfareorders'
	id = db.Column(db.Integer, primary_key=True)
	timestamp = db.Column(db.Integer)
	address = db.Column(db.String(1024))
	price = db.Column(db.Float)
	details = db.Column(db.String(4096))
	status = db.Column(db.Integer, default=1)
	unread = db.Column(db.Boolean, default=True)
	unread_by_user = db.Column(db.Boolean, default=True)
	in_pos = db.Column(db.Boolean, default=False)
	pos_details = db.Column(db.String(4096))
	img = db.Column(db.String(512))
	remarks = db.Column(db.String(1024), default='{}') #json format
	collection_time = db.Column(db.String(512))
	comments = db.Column(db.String(1024))
	token = db.Column(db.String(80))
	promo_id = db.Column(db.Integer, db.ForeignKey('foodfarepromoconfig.id'))
	user_id = db.Column(db.Integer, db.ForeignKey('foodfareusers.id'))
	store_id = db.Column(db.Integer, db.ForeignKey('foodfarestores.id'))
	tags = db.Column(db.String(1024), default='[]')

	# status
	# 1 = received
	# 2 = attending, on its way
	# 3 = complete
	# 4 = cancelled
	# 5 = paid
	# 6 = failed
	# 7 = collected

	store_obj = db.relationship('FoodfareStore', foreign_keys='FoodfareOrder.store_id')
	promo_obj = db.relationship('FoodfarePromoConfig', foreign_keys='FoodfareOrder.promo_id')
	user_obj = db.relationship('FoodfareUser', foreign_keys='FoodfareOrder.user_id')

	# init
	def __init__(self):
		self.timestamp = time.time()

	def get_status(self):
		order_status = {
			1: 'received',
			2: 'attending',
			3: 'complete',
			4: 'cancelled',
			5: 'paid',
			6: 'failed',
			7: 'collected'
		}
		return order_status[self.status]

	def serialize(self):
		data = {}
		for c in self.__table__.columns:
			value = getattr(self, c.name)
			if c.name in ['details', 'tags', 'remarks']:
				value = json.loads(value)
			if c.name == 'status':
				value = self.get_status()
			data[c.name] = value
		return data


class FoodfarePayment(db.Model):
	__tablename__ = 'foodfarepayments'
	id = db.Column(db.Integer, primary_key=True)
	currency = db.Column(db.String(12), default='SGD')
	amount = db.Column(db.Float)
	payment_type = db.Column(db.String(40))
	status = db.Column(db.Integer, default=0)
	payment_token = db.Column(db.String(80))
	user_id = db.Column(db.Integer, db.ForeignKey('foodfareusers.id'))
	timestamp = db.Column(db.Float(asdecimal=True))
	transaction_id = db.Column(db.String(80))
	cancel = db.Column(db.Boolean, default=False)
	order_id = db.Column(db.Integer, db.ForeignKey('foodfareorders.id'))
	mode = db.Column(db.String(80))
	void = db.Column(db.Boolean, default=False)
	bypass = db.Column(db.Boolean, default=False)
	remarks = db.Column(db.String(1024))
	save = db.Column(db.Boolean, default=False)

	user_obj = db.relationship('FoodfareUser', foreign_keys='FoodfarePayment.user_id')
	order_obj = db.relationship('FoodfareOrder', foreign_keys='FoodfarePayment.order_id')

	# init
	def __init__(self, user_id, amount, payment_type, cancel, payment_token=None, transaction_id=None):
		self.user_id = user_id
		self.amount = amount
		self.payment_type = payment_type
		self.timestamp = time.time()
		self.cancel = cancel
		self.payment_token = payment_token
		self.transaction_id = transaction_id

	def get_token(self):
		return self.payment_token

	def get_status(self):
		status_map = {0: 'pending',
					  1: 'completed',
					  2: 'failed',
					  3: 'refunded',
					  4: 'cancelled'} #by client
		return status_map[self.status]

	def serialize(self):
	   return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class FoodfareAd(db.Model):
    __tablename__ = "foodfareads"
    id = db.Column(db.Integer , primary_key=True)
    image = db.Column(db.String(256))
    description = db.Column(db.String(1024))
    timestamp = db.Column(db.Integer)
    ends_at = db.Column(db.String(128))
    active = db.Column(db.Boolean, default=True)

    def __init__(self):
		self.timestamp = time.time()

    def serialize(self):
	   return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class FoodfareWallet(db.Model):
	__tablename__ = 'foodfarewallets'
	id = db.Column(db.Integer, primary_key=True)
	user_id = db.Column(db.Integer, db.ForeignKey('foodfareusers.id'))
	is_primary = db.Column(db.Boolean, default=False)
	method_type = db.Column(db.String(128))
	payment_token = db.Column(db.String(512))
	masked_card_num = db.Column(db.String(128))

	user_obj = db.relationship('FoodfareUser', foreign_keys='FoodfareWallet.user_id')

	# init
	def __init__(self, user_id, method_type, payment_token, masked_card_num):
		self.user_id = user_id
		self.method_type = method_type
		self.payment_token = payment_token
		self.masked_card_num = masked_card_num

	def serialize(self):
	   return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class FoodfareFeedback(db.Model):
	__tablename__ = 'foodfarefeedbacks'
	id = db.Column(db.Integer, primary_key=True)
	body = db.Column(db.String(2048))
	user = db.Column(db.Integer)
	timestamp = db.Column(db.Integer)
	file = db.Column(db.String(1024))


	# init
	def __init__(self, user, body):
		self.body = body
		self.timestamp = time.time()
		self.user = user

	def serialize(self):
	   return {c.name: getattr(self, c.name) for c in self.__table__.columns}


