#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
from requests.auth import HTTPBasicAuth
import hashlib

from datetime import datetime
import json

HEADERS = {'Content-type': 'application/x-www-form-urlencoded'}


def ee_payment(amount, currency, payment_token, success_redirect_url, fail_redirect_url, cancel_redirect_url, merchant_account_id, secret_key, url, psp_name, wirecard_token, bypass, auth, three_d=True):
	request_time_stamp = datetime.utcnow().strftime("%Y%m%d%H%M%S")
	request_id = payment_token
	transaction_type = 'purchase'
	redirect_url = ''
	ip_address = '127.0.0.1'
	payment_method = "creditcard"

	hash_object = hashlib.sha256(request_time_stamp+request_id+merchant_account_id+transaction_type+str(amount)+currency+redirect_url+ip_address+secret_key)
	request_signature = hash_object.hexdigest()

	req_args = [
        "attempt_three_d=true&psp_name={0}&request_id={1}".format(psp_name, request_id),
		"&request_time_stamp={}".format(request_time_stamp),
		"&request_signature={}".format(request_signature),
		"&merchant_account_id={}".format(merchant_account_id),
		"&transaction_type={}".format(transaction_type),
		"&requested_amount={}".format(amount),
		"&requested_amount_currency={}".format(currency),
		"&ip_address={}".format(ip_address),
		"&success_redirect_url={}".format(success_redirect_url),
		"&fail_redirect_url={}".format(fail_redirect_url),
		"&cancel_redirect_url={}".format(cancel_redirect_url)
	]

	if wirecard_token != '' and bypass == 'true':
		token = "&token_id={}".format(wirecard_token)
		req_args.append(token)

	if three_d is False:
		three_d = '&attempt_three_d=false'
		req_args.append(three_d)

	auth = HTTPBasicAuth(auth['username'], auth['password']) #prod
	if 'test.wirecard.com.sg' in url:
		auth = HTTPBasicAuth('wirecardnon3d', 'Tomcat123') #uat

	req_args = ''.join(req_args)
	resp = requests.post(url=url, headers=HEADERS, auth=auth, data=req_args)
	data = {'params': resp.text if bypass=='true' else None,
					'redirect_url': resp.url if bypass=='false' else None}
	return json.dumps(data)
